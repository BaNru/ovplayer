<?php header("HTTP/1.1 418 I'm a Teapot"); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>418 I'm a Teapot</title>
    <style>
        table{
            height: 100%;
            position: fixed;
            width: 100%;
        }
        td{
            font: bold 50vh sans-serif;
            text-align: center;
            vertical-align: middle;
            color: #666;
        }
    </style>
  </head>
  <body>
    
    <table><tr><td>418</td></tr></table>
    
  </body>
</html>