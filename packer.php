<?php
exit;
function packer($src,$out,$f=false){

	$script = file_get_contents($src);

	$t1 = microtime(true);

	$packer = new JavaScriptPacker($script, 'None', true, true);
	$packed = $packer->pack();

	$t2 = microtime(true);
	$time = sprintf('%.4f', ($t2 - $t1) );
	echo '<tr><td>', $src, '</td><td>' , $out, '</td><td>', $time, ' s.</td>',
		'</tr>';

	if ($f == true){
		preg_match("~(\/\/ ==UserScript==.*\/\/ ==/UserScript==)~isU", $script, $header);
		$packed = $header[0].PHP_EOL.PHP_EOL.$packed;
	}

	$packed = str_replace('ovpsrc.generalvideo.ru','ovplayer.ru',$packed);
	
	file_put_contents($out, $packed);
	
	return $time;
}
require 'class.JavaScriptPacker.php';
?>
<!doctype html>
<html>
<head><meta charset="utf-8"><title>***</title><style>
	table {width:80%;margin:0 auto;}
	td {border:1px solid #464646;padding: 2px 10px;}
	tr:hover{background-color:#e5e5e5;}
</style></head><body>
<table>
	<tr><th>out script</th><th>packed in</th><th>time</th></tr>
<?php

	// Плеер
$time =
	packer('ovplayer/gm_firefox/latest/OVPlayer.user.js',	'../ovplayer.ru/ovplayer/gm_firefox/latest/OVPlayer.user.js',true)//;
+	packer('ovplayer/gm_firefox/latest/OVPlayer_fn.js',	'../ovplayer.ru/ovplayer/gm_firefox/latest/OVPlayer_fn.js')//;

+	packer('javascript/install.js',	'../ovplayer.ru/javascript/install.js')//;
+	packer('javascript/player_load.js',	'../ovplayer.ru/javascript/player_load.js')//;
+	packer('javascript/FileSaver.js',	'../ovplayer.ru/javascript/FileSaver.js')//;
+	packer('javascript/image-crop.js',	'../ovplayer.ru/javascript/image-crop.js')//;
	
	// Модули
+	packer('ovplayer/gm_firefox/latest/modules/sibnet.js',		'../ovplayer.ru/ovplayer/gm_firefox/latest/modules/sibnet.js')//;
+	packer('ovplayer/gm_firefox/latest/modules/vk.js',			'../ovplayer.ru/ovplayer/gm_firefox/latest/modules/vk.js')//;
+	packer('ovplayer/gm_firefox/latest/modules/sibnet.js',		'../ovplayer.ru/ovplayer/gm_firefox/latest/modules/ivi.js')//;
+	packer('ovplayer/gm_firefox/latest/modules/vk.js',			'../ovplayer.ru/ovplayer/gm_firefox/latest/modules/my-hit.js')//;

	// Страницы
+	packer('add.html',			'../ovplayer.ru/add.html')//;
+	packer('config.html',		'../ovplayer.ru/config.html')//;
+	packer('index.html',		'../ovplayer.ru/index.html')//;
+	packer('player.html',		'../ovplayer.ru/player.html')//;
+	packer('playlist.html',		'../ovplayer.ru/playlist.html')//;
	
	//CSS
+	packer('style/panel.css',		'../ovplayer.ru/style/panel.css')
+	packer('style/style.css',		'../ovplayer.ru/style/style.css')
+	packer('style/index.css',		'../ovplayer.ru/style/index.css');


?>
<tr><th colspan='3' style='text-align:right;'><?php echo $time;?> s.</th></tr>
</table></body></html>