/*!
	Module iviParse 1.01
	for UserSripts OVPlayer (OnlineVideoPlayer)
	@date		2013-10-30
	@autor		BaNru
	@license	MIT License
	@other		---
*/

if(!services){var services = new Object();};

services.ivi = {

	parseFn : function(url) {

		var videoId = url.match(/(\/|=)(\d+)/)[2];
		url = 'http://api.digitalaccess.ru/api/json/?r='+(Math.random() * 1000);

		setTimeout(function() {
			GM_xmlhttpRequest({
				method: "POST",
				headers : {
					Referrer : "http://www.ivi.ru/video/player?videoId="+videoId
				},
				data: JSON.stringify({"method":"da.content.get","params":[videoId,{"campaignid":"","_domain":"www.ivi.ru","sourceid":"","utmfullinfo":"","watchid":videoId+"_"+unsafeWindow.config['ivi']+"_"+(new Date().getTime()),"_url":"http://www.ivi.ru/video/player?videoId="+videoId,"site":"1","uid":unsafeWindow.config['ivi'],"referrer":"http://www.ivi.ru/video/player?videoId="+videoId,"contentid":videoId}]}),
				url: url,
				onload: function(response) {
					
					var json = JSON.parse(response.responseText);
					//unsafeWindow.mlog(json);
					document.getElementById('player').setAttribute('style','background-image: url("'+json.result.preview[json.result.preview.length-1].url+'")');
					
					var list = new Array();
					for (var i = 0; i < json.result.files.length; i++) {
					
						switch (json.result.files[i].content_format) {
							case 'Flash-Access':
								var c_bitrate = '800';
								var label = '240';
								var isD = false;
								break;
							case 'MP4-SHQ':
								var c_bitrate = '1600';
								var label = '360';
								var isD = false;
								break;
							case 'MP4-hi':
								var c_bitrate = '2000';
								var label = '480';
								var isD = false;
								break;
							case 'MP4-low-mobile':
								var c_bitrate = '2400';
								var label = '720';
								var isD = false;
								break;
							case 'MP4-mobile':
								var c_bitrate = '2400';
								var label = '000';
								var isD = true;
								break;
							case 'MP4-lo':
								var c_bitrate = '2400';
								var label = '000';
								var isD = false;
								break;
							default:
								var c_bitrate = '400';
								var label = '240';
								var isD = false;
								break;
						};

						list[i] = {url: json.result.files[i].url, bitrate: c_bitrate, label: label, isDefault: isD };

					}
					unsafeWindow.mlog(list);
					unsafeWindow.load_player(list);
				},
				onerror: function(response) {
					alert(response);
				}
			});
		}, 0);
	}
};

services.ivi['page'] = /^https?:\/\/www.ivi.ru\/(watch|video)(.*)?(\/|=)(\d+)/;

services.ivi['pageFn'] = function() {

	if (/^https?:\/\/www.ivi.ru\/(watch|video)(.*)?(\/|=)(\d+)/.exec(window.location)){
		unsafeWindow.get_postMessage();
		
		var videoId = window.location.href.match(/(\/|=)(\d+)/)[2];
		var url = encodeURIComponent('http://www.ivi.ru/video/player?videoId='+videoId);
		
		if($('div.poster div.image img').get(0) != undefined){
			var title = encodeURIComponent($("meta[property='og:title']").attr("content"));
			var img = encodeURIComponent($('div.poster div.image img').attr('src').replace('/172x264/',''));
		}else{
			GM_xmlhttpRequest({
				method: "GET",
				url: 'http://www.ivi.ru/watch/'+videoId,
				onload: function(response) {
					var title = encodeURIComponent(response.responseText.match(/<meta property="og:title" content="(.*)"\/>/));
					var img = response.responseText.match(/<div class="image"><img src="(.*)\.(jpg|png|gif)\/172x264\/"/);
					img = encodeURIComponent(img[1]+'.'+img[2]);
				},
				onerror: function(response) {
					alert(response);
				}
			});
		}

		if(typeof xcnt_product_id == undefined){
			panel({
				btnfn:{
					name:'addPlaylistFrame',
					arg: '\''+title+'\',\''+url+'\',\''+img+'\'',
					class: 'addbtn'
				},
				title:'Добавить в OVPlayer'
			});
		}else{
			panel({
				go:{
					page: 'add',
					get: 'title='+title+'&url='+url+'&img='+img,
					class: 'addbtn',
				},
				title:'Добавить в OVPlayer'
			});
		};
		
		panel({
			go:{
				page: 'player',
				get: 'url='+url,
				class: 'showbtn'
			},
			title:'Смотреть в OVPlayer'
		});
		
		if (/^https?:\/\/www.ivi.ru\/video\/player\?videoId=\d+/.exec(window.location)){
			//var e = document.getElementsByTagName("embed")[0].setAttribute("wmode", "transparent");
			document.getElementById('player_panel').style.opacity = '1';
		}
	};
};

services.ivi['favicon'] = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAMCAYAAACJOyb4AAAACXBIWXMAAAsSAAALEgHS3X78AAABhElEQVR4nKWSTShEURSA7/BsZYGyEGUlrG0oKc2UlSiEspGFJEtRNjaUv1IKG8XCT0pSk7DBQkqSspKFoiazIRJmnu/0TuPd26y49c27754z3z2ddzzfRD+NMZcwCfvGj8v731YkZrk8fvKgFvrgBJ7/LHdcngpXYJ2qU/8QG9cl8jY2CSslEpMK0pn3bJf+5uRAipy06xL5OYlrPOcIJPW8GwZEC5vEt4ndq1Rk9TAIpfACXZBwXSIvhzGoI9CuNx/CNHxAJawSayT2zb4BluAAWmE+VK3lEvk7HMGjVjxD8gPBcfZV0ALF0MnZBs8JqIBC2IKpULMsl8h7oBf6YTGUuADNupee50MRXJlg3EQ0SyFPof9YLpHH4A5utcfB8uM+lb6yi0KuypqgGsr0AndsLZfIZSaT2sMdJ/lMk2u0cmnJrgk+6BAFfDn5lsvTQ0m6gVMrVT5gJDZigr53QAkUwCjsmewr4xL5NQwjOs6a6scvuOCNnbRIpkVGb1nn2l2W6we1CpjFBy/BuAAAAABJRU5ErkJggg==';