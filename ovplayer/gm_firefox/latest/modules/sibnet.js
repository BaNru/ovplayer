/*!
	Module sibnetParse 1.01
	for UserSripts OVPlayer (OnlineVideoPlayer)
	@date		2013-08-19
	@autor		BaNru
	@license	MIT License
	@other		Во глубине сибирских руд
				Храните гордое терпенье,
				Не пропадет ваш скорбный труд
				И дум высокое стремленье.

				Несчастью верная сестра,
				Надежда в мрачном подземелье
				Разбудит бодрость и веселье,
				Придет желанная пора:

				Любовь и дружество до вас
				Дойдут сквозь мрачные затворы,
				Как в ваши каторжные норы
				Доходит мой свободный глас.

				Оковы тяжкие падут,
				Темницы рухнут — и свобода
				Вас примет радостно у входа,
				И братья меч вам отдадут.
*/

if(!services){var services = new Object()};

services.sibnet = {
	parseFn : function(url) {

		setTimeout(function(){
			GM_xmlhttpRequest({
				method: 'GET',
				url: url,
				onload: function(response){
					var str = response.responseText;

					var img = str.match(/'image'\s?:\s?'(.*)'/)[1];
					unsafeWindow.mlog("Картинка: "+img);
					document.getElementById('player').setAttribute('style','background-image: url("'+img+'")');
					
					var video = str.match(/'file'\s?:\s?'(.*)'/)[1];
					unsafeWindow.mlog("Ссылки: "+video);
					var list = new Array();
					list[0] = {url: video, label: '???'};
					unsafeWindow.mlog(list);
					
					unsafeWindow.load_player(list);

				},
				onerror: function(response) {
					alert(response);
				}
			});
		}, 0);

	}
};

services.sibnet['page'] = /https?:\/\/video.sibnet.ru\/shell.php/;

services.sibnet['pageFn'] = function() {

	if (/https?:\/\/video.sibnet.ru/.exec(window.location)){
		unsafeWindow.get_postMessage();

		//jwplayer().getPlaylistItem()['image'];
		//jwplayer().getPlaylistItem()['file'];
		//title = encodeURIComponent(document.getElementById('player_container').childNodes.item(1).getAttribute('alt'));

	//	unsafeWindow.setup = function(e){
	//		alert(e.width);
	//	};

		if(document.getElementsByClassName('video_name')[0] !== undefined){
			if (document.getElementsByClassName('video_name')[0].getElementsByTagName('h2')[0]){
				title = encodeURIComponent(document.getElementsByClassName('video_name')[0].getElementsByTagName('h2')[0].textContent);
			}else if (document.getElementsByClassName('video_name')[0].getElementsByTagName('h1')[0]){
				title = encodeURIComponent(document.getElementsByClassName('video_name')[0].getElementsByTagName('h1')[0].textContent);
			}else{
				title = '';
			}
		}else{
			title = '';
		};

		//if(){ // NoScript
			//var str = document.body.innerHTML.match(/'file':'(.*\.(mp4|flv))',/)[1];

			var url = encodeURIComponent('http://video.sibnet.ru/shell.php?videoid='+document.body.innerHTML.match(/'counter.videoid'\s?:\s?'(.*)'/)[1]);
			var img = encodeURIComponent(document.body.innerHTML.match(/'image'\s?:\s?'(.*)'/)[1]);

		//}else{
			//str = document.getElementById('player_container').parentNode.nextSibling.nextSibling.textContent;
			//var str = document.getElementById('player_container_wrapper').nextSibling.nextSibling.textContent;
			//var url = encodeURIComponent('http://video.sibnet.ru/shell.php?videoid='+str.match(/'counter.videoid'\s?:\s?'(.*)'/)[1]);
			//var img = encodeURIComponent('http://video.sibnet.ru'+str.match(/'image'\s?:\s?'(.*)'/)[1]);
		//}

		if(typeof jwplayer === 'function'){
			panel({
				btnfn:{
					name:'addPlaylistFrame',
					arg: '\''+title+'\',\''+url+'\',\''+img+'\'',
					class: 'addbtn'
				},
				title:'Добавить в OVPlayer'
			});
		}else{
			panel({
				go:{
					page: 'add',
					get: 'title='+title+'&url='+url+'&img='+img,
					class: 'addbtn',
				},
				title:'Добавить в OVPlayer'
			});
		}
		
		panel({
			go:{
				page: 'player',
				get: 'url='+url,
				class: 'showbtn'
			},
			title:'Смотреть в OVPlayer'
		});

	};

};

services.sibnet['name'] = 'video.sibnet.ru';
services.sibnet['favicon'] = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFcAAAAMCAYAAAAXgSFYAAAACXBIWXMAAAsSAAALEgHS3X78AAAIN0lEQVR4nLVXC1BU5xX+72tfvHQBQVR0BQEFQQZfAWnFhsgUo4nKUIw2MtWSRKNNG3XSjpqGZEZrY3wkJHVMK6ZRBxPFaEzUYPGRAALqImsQXZb3Y92F3eW1u/fV818eVVgWOpP8M3f23v95zne+851/aVEU0U/RVm/5cnO5ruUwTZEnHl5+7aWfZNOfsf3rjPalnQev/dvbU375/tdZS3+OM+iBlz1Hfpjv5PipJEEQ7hbwgkj4jVPeTV08Q/9ZfsUyVhC9MlIjv2FoksXjDEPyn5+v9HtU35FEkcP3kmJJINPypBlFMREBvU+OHcm7E95i7IqkKIIeuq5vrSiSJFG/YfWcWwF+noOsKL7bpLx0U7+AosgJI9ktiCIhZ+jWtJSZN6dPGcfjPmydIIiB2/cVpAHIUteQZQTPi9as9HmXuqjbU+61Fj5DE4zgDh8JI5EVI/wTy+hmYye9fd/VXQXFhjeBdcrRFnK8gEKmqLfNmRmQk3Oy/CO7kw9aEB2UyFCkA4/DHmyVwRx59PSdPHh3vQnAcvxsxXe7NyduWLt8dh3uWrvt3Gs3yurfARB8RznfWaJtzv30vWVb/dWq3g9yS6YfOl56lOOEpGHQDD9X+Oyre7kf7055RcZQEkgQr+hTX9/Pw4Ouchj2fbQqOTrCKNfGX9UfPSmjlH0OwGE46/u42Pc9cAjuv910sYJeteXLzDZT9w4fD7kMgKriBcEEM6gR7RMRqVLQDQo5zWkmjbvi4PggDyVjFkQU1j8FI2qB5/v+96FNAIZFAojPHsy9tR/AXfXn/f9ZXFBkOOCpkjEOJ68D8ywu1mIPaE+lLFZb1bZxU/a3+rSlM/92MLf07zKGTAJG9zhZvnKIp0+uJZUKOsrWZc88cV53NiFuihMPADhWsF/X1eMkXAQH9+jxCyQTj4FlSDkwk8NxQiraG7GCEzMV0SQjTecFFhEkhZx8bxT92Ny9HlJaxnJCU1Z6bMpbWQl1IwGL28kLOjpMoxbDNb7CoZ1Lt+JMDZ2qtoKkLMLjLCvI50ZNrJgRPH4JMVxisJOivr4jBRj0hbG9Z0nhrboA7YO2VAgW4+0lvwAp/zKA3DkCuMSxM9ptdc3W7Ioq48oIje8xmiIWcpwozo8O2rryufBPXakaSIFEykVrck+B7KSX6VoiAvw89DizQMbKsrcuXmbttAuuJBH8E6LCffirNSLJ8nY00z8BJU5bh8w99chHEYjKG79CzwSnozO6bGR1tqPMuPeRrvU7VNKUz2FtKwQdjYfIT6h8+Hjvlvcut7iIvBRFSJHasGnqzzOWRRrvVRu91/wp/wTLC5p//PXX6XKGkpiAZWPFkjDsjHOkAJVom7THzmptcKYH6LI3OKlyOnm0MGbSnVcz4tpHWofbxeuPimoaLUhBk8p+GSPBfm7qJJ+iakN7WKupK4CkyKd08UZ5A855qtfOBWPDPBQMq1QwBE5f0FQEgXEE+Xu6reygzaDbPPKS+aJw/0RUXJ+HTle+jcLUC9EM/3gko1QAWgcK8Z2PWmxVMFdA9Ie7Uva9k3MjrKHFtvp6WX063mSkhiN9jhcybV2O51c8G27meSEcAA8BIz3w+e6Me7JB6uOgDojVwIMbM9pasG9gzuA64JvocHJM8d3Gd2saOl6gXGo9gaBQIpbjrS+vjL6ilNORHN93LMvyeAE/FttxQCCWAO5pYG8zEtS89C3AciA54gSHFATc6JTEEEvwRJ/1H58sv/Cwrj1qhCIE5BADDY2WdNC12QXFtZuSE6bvBuax/YkkadrA3NEMBOd59D9tHK0Mjdak9X22EA3w+RAn2dBJAAqhUsqaITP2Z66M0YG8xA7YOzXIZ0zA9p2GCxkv6S5F0hLYJFxuQFmRivFGcspjcKp05ZkV6td9eOfSXHd7VjxoYzL+mD8HNosBjQ6FM566LoFm2/EvMEtz5QeDR3K8pnukvepbrBPhxwPmgoqIjjE7NsRNyBwW9nAAurS10xH67h8Wvwl9fwGQh6UfFGqUtGBa16C9cOvA10IwYHKxtskPJMk0lkOxvjh5yVVc5FCzrRrZ7G0oI2avpMNQyCQm40an/v5UFvz+EkfEDYUEU0ePP7A2EhOFoSktODDIDigEVOysgBIwsp2kiIQd+wouH/D3NCDXtwWxodU2H9irhMp/fXKgVxMA7PJe664BiERIsNrsO15V2GGzr7tZ3pBjNHdfYxgK2+XSlQO5pXSPndVe+eeaPZGh/iVyGWmE4Mx6PfvSNxPUKn2/vSRo9vXUX4Rcyr9anQ2Z3pCz6/ltQCgR62rV4+/R4aJ1yOYwgs4qkLm3EX1SsgEFeoWiBkslZCUjAcxQCor+scacABtmjOYMFB6cehxE/FsoaB/5eMppMIxAfdFndmyMr71WWv9GVY15b2e3M97SaY53txcE5+7v0ua8ERqs5kG3/29pwDq5KG4ymCDufPvDaxpI0YT7j0wZo/3fBL0fDz974mMn173wq/BN5wqq37fY7HPNlt65g/ZRpDMuMvD2j3pzmiiIVXaHsB3JkYgvEw6uG/WwNkkSMB0puIKZgLFt3TWSNGDdJQgSCEcZaIiIFvYLGoM/OIdvvr523qHfrpjdUfHA6Al9xfA0gYMdeMLFI785vvzVPAPcOl6BfXHquypyBDioTU7QHNqybl5tf1816rsX14zBDnybKITHANWfXP9idN3j9u4XPzl1ezNNk5go7gJFQTTKBz72v5X8xX29qba6tn0j2IsxwLGhANxSIIAJ+s4DuAbAFPthxOcCcDxNPJ2QGGBqoBYTFC5o3DhFwOH/Atkl4waP9OL6AAAAAElFTkSuQmCC';