/*!
	Module myvi.ru 1.0
	for UserSripts OVPlayer (OnlineVideoPlayer)
	@date		2014-03-11
	@autor		BaNru
	@license	MIT License
	@other		---
*/

if(!services){var services = new Object();};

services.myvi = {

	parseFn : function(url) {
		setTimeout(function(){
			GM_xmlhttpRequest({
				method: 'GET',
				url: url,
				//binary: true,
				//overrideMimeType: 'text/plain; charset=x-user-defined',
				onload: function(response){

					var data = Base64.decode(decodeURIComponent(response.responseText.match(/playerData=(.*)&amp;player/)[1]));
					
					var parser = new DOMParser();
					var doc = parser.parseFromString(data, "text/xml");
	
					//var title = doc.getElementsByTagName('Title')[0].innerHTML.replace("<![CDATA[", "").replace("]]>", "");
					//var Video = doc.getElementsByTagName('Video')[0];
					//var duration = Video.getAttribute('duration');
	
					var VideoSource = doc.getElementsByTagName('VideoSource')[0];
					//var filesize = VideoSource.getAttribute('filesize');
					//var bitrate = VideoSource.getAttribute('bitrate');
					var VideoUrl = VideoSource.getAttribute('url')
									   .replace('uid=&','').replace('puid=&','')
									   .replace('ef=&','').replace(/&/g,'%26')
									   .replace(/=/g,'%3d');
					var img = doc.getElementsByTagName('Poster')[0].getAttribute('url');
					document.getElementById('player').setAttribute('style','background-image: url("'+img+'")');

					mlog(img,{n:'Картинка'});
					mlog(VideoUrl, {n:'Ссылка'});
					
					var list = new Array();
					list[0] = {url: VideoUrl, label: '???'};
					unsafeWindow.mlog(list);

					unsafeWindow.load_player(list);
				},
				onerror: function(response) {
					alert(response);
				}
			});
		}, 0);
	}
};


services.myvi['page'] = /^https?:\/\/(www.)?myvi.(tv|ru)\/(content|preloader.swf|watch|player|player|embed|html|video|api|\/){2,6}/;


services.myvi['pageFn'] = function() {
if (services.myvi['page'].exec(window.location)){
	if (/^https?:\/\/(www.)?myvi.ru\/watch/.exec(window.location)){
		var title = document.querySelector("meta[property='og:title']").content;
		var img = document.querySelector("meta[property='og:image']").content.replace(/_th(\d).jpg/,"_tm$1.jpg");
		var url = 'http://myvi.ru/player/embed/html/'+encodeURIComponent(parseUrl(document.querySelector("meta[property='og:video']").content)["id"]);
	} else if (/^https?:\/\/myvi.ru\/player\/embed\/html/.exec(window.location)){
		var videoID = window.location.href.split("/")[6];
		var url = 'http://myvi.ru/player/embed/html/'+videoID;//+'?sig=666';
		var img = 'http://fs.myvi.ru/thumbnail/'+videoID+'/default_hq.jpg';
		var title = document.querySelector('title').innerHTML;
		//var data = Base64.decode(decodeURIComponent(parseUrl(document.querySelector("param[name='flashvars']").value)['playerData']));
		//var parser = new DOMParser();
		//var doc = parser.parseFromString(data, "text/xml");
		//var title = doc.getElementsByTagName('Title')[0].innerHTML.replace("<![CDATA[", "").replace("]]>", "");
		//var img = doc.getElementsByTagName('Poster')[0].getAttribute('url');
		//var VideoSource = doc.getElementsByTagName('VideoSource')[0];
		//var url = VideoSource.getAttribute('url');
	} else if (/^https?:\/\/myvi.tv\/embed\/html/.exec(window.location)){
		var videoID = parseUrl(document.querySelector("param[name='flashvars']").value)['id'];
		var url = 'http://myvi.ru/player/embed/html/'+videoID;//+'?sig=666';
		var img = 'http://fs.myvi.ru/thumbnail/'+videoID+'/default_hq.jpg';
		var title = "";
	}/* else if (/^https?:\/\/myvi.tv\/content\/preloader.swf/.exec(window.location)){
		var videoID = parseUrl(location.search)['id'];
		var url = 'http://myvi.ru/player/embed/html/'+videoID;//+'?sig=666';
		var img = 'http://fs.myvi.ru/thumbnail/'+videoID+'/default_hq.jpg';
		var title = "";
	}*/
	var setIntervalPP = setInterval(function(){
		if(document.getElementById('player_panel')){
			document.getElementById('player_panel').style.opacity = '1';
			clearInterval(setIntervalPP);
		}
	}, 200);

	panel({
		btnfn:{
			name:'addPlaylistFrame',
			arg: '\''+title+'\',\''+url+'\',\''+img+'\'',
			href: 'http://'+BASE_URL+'add.html?url='+encodeURIComponent(url)
					+'&title='+title+'&img='+img,
			class: 'addbtn'
		},
		title:'Добавить в OVPlayer'
	});
	panel({
		btnfn:{
			name: 'document.location.href',
			arg: 'http://'+BASE_URL+'player.html?url='+encodeURIComponent(url),
			href: 'http://'+BASE_URL+'player.html?url='+encodeURIComponent(url),
			class: 'showbtn'
		},
		title:'Смотреть в OVPlayer'
	});

}	
};


/*
services.myvi['page'] = /^https?:\/\/myvi.(tv|ru)\/(player|player\/embed)\/(html|video)/;

services.myvi['pageFn'] = function() {

	if (/^https?:\/\/myvi.(tv|ru)\/(player|player\/embed)\/html/.exec(window.location)){
		unsafeWindow.get_postMessage();
		
		var videoId = window.location.href.split("/")[6] || window.location.href.split("/")[5];
		var url = 'http://myvi.ru/player/api/video/getFlash/'+videoId+'?sig=666';

		console.log(url);
		
		GM_xmlhttpRequest({
			method: "GET",
			url: url,
			binary: true,
			/*headers: {
				"User-Agent": "Mozilla/5.0",
			},*\/
			overrideMimeType: 'text/plain; charset=x-user-defined',
			onload: function(response) {
				var data = Base64.decode(response.responseText.match(/<playerData>(.*)<\/playerData>/)[1])
				var parser = new DOMParser();
				var doc = parser.parseFromString(data, "text/xml");

				var title = doc.getElementsByTagName('Title')[0].innerHTML.replace("<![CDATA[", "").replace("]]>", "");

				var Video = doc.getElementsByTagName('Video')[0];
				var duration = Video.getAttribute('duration');

				var VideoSource = doc.getElementsByTagName('VideoSource')[0];
				var filesize = VideoSource.getAttribute('filesize');
				var bitrate = VideoSource.getAttribute('bitrate');
				var VideoUrl = VideoSource.getAttribute('url');

				var img = doc.getElementsByTagName('Poster')[0].getAttribute('url');

				console.log ( 'Заголовок: '+title , ' Длительность: '+duration , ' Размер: '+filesize , ' Качество: '+bitrate);
				
				panel({
					btnfn:{
						name:'addPlaylistFrame',
						arg: '\''+title+'\',\''+url+'\',\''+img+'\'',
						class: 'addbtn'
					},
					title:'Добавить в OVPlayer'
				});
			},
			onerror: function(response) {
				alert(response);
			}
		});
		
		panel({
			go:{
				page: 'player',
				get: 'url='+encodeURIComponent(url),
				class: 'showbtn'
			},
			title:'Смотреть в OVPlayer'
		});
		
		document.getElementById('player_panel').style.opacity = '1';

	};
};
*/
services.myvi['favicon'] = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAMCAYAAAC0qUeeAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gMMAhMEjJBDcgAAAM9JREFUKM+F0S1OQ1EQxfHfLZDmIUHgSsoGyg5wCDD1iLIQFCshSFJxFYKkGMIC7gYQNyhIcHwkpC1mXihPlGPO3Dn/zCR35FJTLvVBKJf6x6N+zKWmlEsd4gn7eMW3X21hB8842ESDOe5xia8VuMFF5E3Kpb5j2//66KEfj0PcdoC76EO/13bHo0HBKV5i7RtOog96nUnHGGIj/Gg17MJT7I1Hg4RdXK+D5/GNwhfrYFjkUpdYduEUAVzhLA7R6hM3mLSTZxGcd8D2KJOoZz8tUD1Y7dvb0QAAAABJRU5ErkJggg==';