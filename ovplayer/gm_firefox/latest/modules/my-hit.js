/*!
	Module myHitParse 1.01
	for UserSripts OVPlayer (OnlineVideoPlayer)
	@date		2013-10-30
	@autor		BaNru
	@license	MIT License
	@other		Чтобы заработал модуль - необходимо отключить в настройках Firefox'а передачу Referer в Header'е.
				Для этого надо вбить в адресной строке about:config,
				Найти и изменить на "0" параметр в строке Network.http.sendRefererHeader
				Либо установить плагин https://addons.mozilla.org/ru/firefox/addon/refcontrol/
				для контроля Referer в Header'е и добавить в исключения только сайт OVPlayer'а
*/

if(!services){var services = new Object();};

services.myHit = {

	parseFn : function(url) {

		var id = url.match(/[0-9]{2,9}/)[0];
		
		setTimeout(function(){
			GM_xmlhttpRequest({
				method: 'GET',
				url: 'https://my-hit.org/film/'+id+'/online',
				onload: function(response)
				{
					var str = response.responseText;

					var img = 'https://my-hit.org/images/film/poster_main/'+id+'/poster_main.jpg';
					unsafeWindow.mlog("Картинка: "+img);
					document.getElementById('player').setAttribute('style','background-image: url("'+img+'")');

					var video = str.match(/url:\s?'(https?:\/\/[a-z]-[0-9]{1,5}\.my-hit\.org\/data[0-9]\/previews\/[0-9]{1,9}\.flv\?id=.*)',/)[1];
					
					unsafeWindow.mlog("Ссылки: "+video);
					var list = new Array();
					list[0] = {url: video, label: '???'};
					unsafeWindow.mlog(list);
					
					unsafeWindow.load_player(list);

				},
				onerror: function(response) {
					alert(response);
				}
			});
		}, 0);
	}
};

services.myHit['page'] = /https?:\/\/my-hit.org\/film\//;

services.myHit['pageFn'] = function() {

	if (/https?:\/\/my-hit.org\/film\//.exec(window.location)){
		unsafeWindow.get_postMessage();
	
		var title = encodeURIComponent(document.title.replace(/( - смотреть онлайн)|( - скачать бесплатно)|( - создатели)|( - постеры)|( - кадры)|( - рецензия)|( - съёмки)|( - обои)|( - трейлеры)|( - саундтреки)/,''));

		var id = location.pathname.match(/[0-9]{2,9}/)[0];
	
		var url = encodeURIComponent('https://my-hit.org/film/'+id+'/online');
		var img = encodeURIComponent('https://my-hit.org/images/film/poster_main/'+id+'/poster_main.jpg');

		if(typeof jwplayer === 'function'){
			panel({
				btnfn:{
					name:'addPlaylistFrame',
					arg: '\''+title+'\',\''+url+'\',\''+img+'\'',
					class: 'addbtn'
				},
				title:'Добавить в OVPlayer'
			});
		}else{
			panel({
				go:{
					page: 'add',
					get: 'title='+title+'&url='+url+'&img='+img,
					class: 'addbtn',
				},
				title:'Добавить в OVPlayer'
			});
		}
		
		panel({
			go:{
				page: 'player',
				get: 'url='+url,
				class: 'showbtn'
			},
			title:'Смотреть в OVPlayer'
		});

	};

};

services.myHit['favicon'] = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAF4AAAAMCAYAAADrlAqSAAAACXBIWXMAAAsSAAALEgHS3X78AAAJyElEQVR4nK1XC3BU1Rn+9u7dV7KbB9mEvAh5kARIgBIIUHwMFMpEHuI0CII8KigWamk7dWwrOjDj2KGopVaLEUULAuWhICUqFmmEtCSEtwRIQiBhm5D3O5t9ZHdvv3t3k+waU+zYf+YmZ8/5z3/O//3PI0qd1flQG6KgkMeD7toduPynDzBnVy8qDuQifsZvAJUElUoFR0c77p55AqGJP0J42iqo1B7YWhpRnb8GWc/WQ0Oem5/OQ0TG8xD1gLOrAw0XNyHt0RL4U9OVPATHZENyC+iq+Quis1/vX+u++wYE9XRIHqC28B2kLc5D5cezEDt9K+8A9DRbcH7LBqTm5iB62k+4w42hSHKJaL7+AUbO3hYw/48NRmSumggxeD3vMZp3FcgrwdZqo/4HYGvcjeSHWxXetjIj+Q5BrTPLEn0SeBFJgttJHTsr0XbzDZx67hzWVLiU1Wu7RsCcuQLB0QuhD9cRJ5Ui39HhREfVYVhr3hURFBMOtTbbJ9AFZ/dKhKd/yHEvwb0fQcMn919Yra9EzDQbmisOI8qUC03wgzBEWuFxLuXqNhzbrIF5/JMwxk2FjJyEFxES/9UgQIKiQvll0dCEza4NWDPGmKiXd00bMkyZ0wQN9/IrFE65eq4l+80NQZTR0/hhwFSeWcTc/U8jLG09tKZIzpi8CzqOgoHg2DiCH4+agk2In2lFULQErTEWKnH8EIdk8x6JyNk5TzYTzv3OjIw1r3FuFvUYNsBG+SLlG6Li0aQNEeFxeQi8ghNUgsiDhyFuhohDs0U8ckSvGJmBQC+Uedzo7dYgdX4Nqj7djYTZ8dybDFNCLoo2F2DM96fQgyYoGtuaT6Fs79+Q9Uv7YDzcHuW/JMvudQWuuVwQNN41yePxzQ54tSS5IBoAe/tV2FsPcoJrgo6GeJB3pFd6quHoPMtpD8/RMCIbAuTP/WsUYu9bThnJ3GuBtf4VemwDTHFGhCSlUcZajh9FZ1UzubfAZSNgBjfUIt3S3o2G86+gu6aUDpuBWEacWhdL5xsJR9FijFPtQGHFOoK7SAkKZ2cRGi+eoPH/zSwwE2GpD1FmFEISc8T+C/XwHLUA6M2RsN6diYQZ1cQ8G709VJJrppF9inv/j533Hhot2TCNWA1d6DRkrN4IfRg9Q0ikwmUMqe0E/foQXuIjFcHxROLqzmwqrFKMm7ooSAH+v5EuRI1Rj+zjaJ/yu/LIVIyY+QB0YQTH4YS95RmEpjQrawmzAvdqjOuZWmKUsb01D/96YTvm7OxQfreVL2GUr2VqSEBU1ngcyw3CnHel/r0ep4Su6kKMXl6AdarP8ce2hQrwQAgjwoztz6XRCDlKWpY8ZUypf2Cq9kZcyZZ8JOWcRq/VheZrNQPAO3m2vQWIMUfTg7MooBuibhy6ammUhgHg+8hGC5Tvfw8JPyRvxCQaYJGSg8lNbylGzNQj/UYaEnca2pSwCinhK30TdF7NPVD/BhI0an+pjEJxSN7wdFpHNVy5m7W+ph90mVquXyHwvRwxBagmIyIzA7rwy3A7vOtqnQfmcbPRcGEMttyOowG9oLjsdYyMAxg5JxmCONE71+OAOeOYMi7alMp0lUgHKaWBBIgaDFxQ1BawAHzM0UswxifSEB0QtPJhr0IMkjmeHaTEnc8vImLsO/T4DB4oM7lgb/snWsvyqJiE6uPBMJj1UOm8xdll60VMdqc/ZMzfsfy+PcjfleRC2kf2FkfAWnhqHQH3pTUpAcGRERgoqDIWIYgY97ySlgW1R4nYXmsB2iv3sdhXovVGOnHwpkdZ1/piDZIWOJC+eC0j8BlFlsTUKLlv+QFv6GKoFnKDjUVnGXmWKQfYGm7QEEb/8/tpzvsu7Mvai9yCeQR/IQWKNNhnSJ5/Vlk3RC5ESMJvaUD5MiJlX0aWaiVO1/iE8V9vz1Xm+UrI4eJ2BPMOY+ix8d8Z4KFIrmkDOgdGRnuVkblYpUQepLusFdav7XVx7gKdNAqCMYr6GtF8pQQlvz+AhUflLk6uhwLk+BM0AiIneI3oshvYAfXwPBfUmijqq/c7mAnW8kUv0sw2tkEMNpO84RbbxzqkLRkzpCLLLvbA1uT1HDl8e5rcLB7eNUO4mqGaSS/w/lapb2GSXN59VpQN21m1iyH9mhIRmxCMjd1/JvAr/2dAvy1Z65yKbnJaDE02BawZhmVQB42yJmjOwdrwFRytGqUbkcllt+HWR5thntiCqIl7CFk6Ir+3GJOUWrabOb2DBbSMETyRtUSP28emYMyKU2i79Rb37uX8PHZ9G2XN/YDn+Pxb1UhfeoQ/fiHDxAuUo67oJEblDtVK+cib3L1Dv3RrOXmWHryec3J4C/TucsTDRuUGwl2t8x2vFIRuxav+X1Tz5TqYJ8xSakn7zfMYPnkL8/NJdi8PK92YNnQK2iosbJlPofH8CBZUFkZBvhuLqMWCma93oKcuuB948N5BMRLT5QW0lpewHjDPByUx7/8Upe8fV7w/eUEhGcfT2dJZWH+FG3t6+Aa5gfqiJERlpyh3oRzRG1bKUSKeKu9Gx+1KdiXeDsNa50DOHieLUB9Qgj/GfuQ36ceQ+VQF/1YEcG5aDhbsb+YfJKufBL+xevCyyo9HNSBCGxLNFJirKKs1NSpzvV1foLN6P8JSHic4qwneDKaMaxjxA8pV3adsllthy9/fZCH2Hdd3JVkQHVR2krrinWytH6BnG6GPSGZ9eByZT2zDjd35BH8aIz2LDcdcPgBTWGhdSF0sMopSICPt7LYSeI+agtrYKnure11xI3vxKgiGMLaEV5S0IR8kSQTQ3UyDDFZccvdwvZWfzdv034vcVvK28HPwC+SXfyo9vFTb7/0um8zfRBtF0inusGYIAXsEQcV5C0cR/ftl8rjsHNdzQs2xN1+Pe9qB+pJX0XRZhbBRc+n1iYzIBVyxMg9X08NPoPbMVvx4gwWXfua7j6dPz+p+R6k6dhXG2I/o8Y9xKo4dzGycfekopr54AlX5QUyf6wh8rFKv1NpwyNHsst/kG6CMhs8TcefEr2l5PXvjm3yl8Tl/6ROGfz2rv4F97UX2x8TJsQOVh8/Qg5pplMAHiUyddzai/tweXtDBl+eVe+LeZdnMfHiQoDgRmhT4sq3+7E0WoUNMS3Ze9LIy113zJZV5jPfSs2u6y/tWfk3iRdSefpIv6RgWri56YhtMS+jd1rdR9UkxgWH3FFKKCB939JR2HF/5MsaueJvP+IfoiZnc1wpt2EE0FluQ/UIXLi3tu4+D7fLP+Wo30VNbCGKpMj/95VY0lb6Exvx8aIiVS66s8GKTNP8oU08hIkZH8hXMyCLwHmcldMPyYTneiPu3dv0HsUTVICd1G6AAAAAASUVORK5CYII=';