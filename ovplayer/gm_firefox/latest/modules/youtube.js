/*!
	Module youtube 1.0
	for UserSripts OVPlayer (OnlineVideoPlayer)
	@date		2013-10-30
	@autor		BaNru
	@license	MIT License
	@other		---
*/

if(!services){var services = new Object();};

services.youtube = {

	parseFn : function(url) {

		var videoId = parseUrl(url)['v'];
		
		// Вариант первый (30 кб)
		// распарсиваем саму страницу с видео
		// находим json
		// ytplayer.config.args
		// в нем
		//   a) args['url_encoded_fmt_stream_map']; коллекция 1
		//   b) args['adaptive_fmts']; коллекция 2
		//   c) args['dashmpd']; ссылка на файл с коллекцией 3
		//
		// ---Вариант второй (240 кб)
		// ---s.ytimg.com/yts/jsbin/html5player-ima-en_US-vflYhChiG.js
		//
		// ---Вариант третий (50 кб)
		// ---s.ytimg.com/yts/jsbin/www-embed-player-vflxGKPQj.js
		//
		// Вариант четвертый (15 кб)
		// http://www.youtube.com/embed/hJXdk4mKM9M
		
		setTimeout(function() {
			GM_xmlhttpRequest({
				method: "GET",
				/*headers : {
					Referrer : "http://www.ivi.ru/video/player?videoId="+videoId
				},*/
				//data: JSON.stringify({"method":"da.content.get","params":[videoId,{"campaignid":"","_domain":"www.ivi.ru","sourceid":"","utmfullinfo":"","watchid":videoId+"_"+unsafeWindow.config['ivi']+"_"+(new Date().getTime()),"_url":"http://www.ivi.ru/video/player?videoId="+videoId,"site":"1","uid":unsafeWindow.config['ivi'],"referrer":"http://www.ivi.ru/video/player?videoId="+videoId,"contentid":videoId}]}),
				url: url,
				onload: function(response) {
				
				document.getElementById('player').setAttribute('style','background-image: url("'+response.responseText.match(/<meta property="og:image" content="(.*)">/)[1]+'")');

var itag = {
// Non-DASH
5:{r:'240p',b:'250',c:'FLV'},6:{r:'270p',b:'800',c:'FLV'},13:{r:'N/A',b:'500',c:'3GP'},
17:{r:'144p',b:'50',c:'3GP'},18:{r:'270p/360p',b:'500',c:'MP4'},22:{r:'720p',b:'2500',c:'MP4'},
34:{r:'360p',b:'500',c:'FLV'},35:{r:'480p',b:'1000',c:'FLV'},36:{r:'240p',b:'175',c:'3GP'},
37:{r:'1080p',b:'4000',c:'MP4'},38:{r:'3072p',b:'4400',c:'MP4'},43:{r:'360p',b:'500',c:'WebM'},
44:{r:'480p',b:'1000',c:'WebM'},45:{r:'720p',b:'2000',c:'WebM'},46:{r:'1080p',b:'N/A',c:'WebM'},
82:{r:'360p',b:'500',c:'MP4'},83:{r:'240p',b:'500',c:'MP4'},84:{r:'720p',b:'2500',c:'MP4'},
85:{r:'1080p',b:'3500',c:'MP4'},100:{r:'360p',b:'N/A',c:'WebM'},101:{r:'360p',b:'N/A',c:'WebM'},
102:{r:'720p',b:'N/A',c:'WebM'},
// DASH (video only)
133:{r:'240p',b:'250',c:'MP4'},134:{r:'360p',b:'350',c:'MP4'},135:{r:'480p',b:'750',c:'MP4'},
136:{r:'720p',b:'1250',c:'MP4'},137:{r:'1080p',b:'2500',c:'MP4'},160:{r:'144p',b:'100',c:'MP4'},
264:{r:'1440p',b:'4500',c:'MP4'},
// DASH (audio only)
139:{r:'a48',b:'48',c:'AAC'},140:{r:'a128',b:'128',c:'AAC'},141:{r:'a256',b:'256',c:'AAC'},
171:{r:'a128',b:'128',c:'Vorbis'},172:{r:'a192',b:'192',c:'Vorbis'},
// Live streaming
92:{r:'240p',b:'225',c:'TS'},93:{r:'360p',b:'750',c:'TS'},94:{r:'480p',b:'1000',c:'TS'},
95:{r:'720p',b:'2200',c:'TS'},96:{r:'1080p',b:'4500',c:'TS'},120:{r:'720p',b:'2000',c:'FLV'},
127:{r:'N/A',b:'N/A',c:'TS'},128:{r:'N/A',b:'N/A',c:'TS'},132:{r:'240p',b:'175',c:'TS'},
151:{r:'72p',b:'50',c:'TS'}
};

					var c = [], list = [];
					var json = JSON.parse(response.responseText.match(/ytplayer.config = ({.*});<\/script>/)[1]).args;
					var url_1 = json['url_encoded_fmt_stream_map'].split(',');
					var url_1_l = url_1.length;
					//console.log(url_1);
					
					for (var i = 0; i < url_1_l; i++) {
						//c[i] = decodeURIComponent(url_1[i].substr(4)).split(';');
						c[i] = parseUrl(url_1[i]);
						mlog(c[i]);
						if (!itag[c[i].itag]
							&& itag[c[i].itag]['c'] != 'MP4'
							&& itag[c[i].itag]['c'] != 'FLV') {continue;}

						if (itag[c[i].itag] != undefined) {
							list.push({
								url: c[i]['url'],
								bitrate: itag[c[i]['itag']]['b'],
								label: c[i]['itag'] + ' / ' + itag[c[i]['itag']]['r'],
								isDefault: 'false'
							});
						}else{
							list.push({
								url: c[i]['url'],
								label: c[i]['itag'],
								isDefault: 'false'
							});
						}
					}
/* потоковое видео
 * возможно пойдет для HTML5
					var url_2 = json['adaptive_fmts'].split(',');
					var url_2_l = url_2.length;
					var ic = 0;
					
					for (var i = 0; i < url_2_l; i++) {
						ic = url_1_l+i;
						c[ic] = parseUrl(url_2[i]);
						
						if (itag[c[ic].itag] != undefined) {
							list.push({
								url: c[ic]['url'],
								bitrate: ""+Math.round(c[ic]['bitrate']/1000),
								label: itag[c[ic]['itag']]['r'],
								isDefault: 'false'
							});
						}else{
							list.push({
								url: c[ic]['url'],
								bitrate: ""+Math.round(c[ic]['bitrate']/1000),
								label: c[ic]['itag'],
								isDefault: 'false'
							});
						}
						
					}
*/
/*					var list = new Array();
					for (var i = 0; i < json.result.files.length; i++) {
					
						switch (json.result.files[i].content_format) {
							case 'Flash-Access':
								var c_bitrate = '800';
								var label = '240';
								var isD = false;
								break;
							case 'MP4-SHQ':
								var c_bitrate = '1600';
								var label = '360';
								var isD = false;
								break;
							case 'MP4-hi':
								var c_bitrate = '2000';
								var label = '480';
								var isD = false;
								break;
							case 'MP4-low-mobile':
								var c_bitrate = '2400';
								var label = '720';
								var isD = false;
								break;
							case 'MP4-mobile':
								var c_bitrate = '2400';
								var label = '000';
								var isD = true;
								break;
							case 'MP4-lo':
								var c_bitrate = '2400';
								var label = '000';
								var isD = false;
								break;
							default:
								var c_bitrate = '400';
								var label = '240';
								var isD = false;
								break;
						};

						list[i] = {url: json.result.files[i].url, bitrate: c_bitrate, label: label, isDefault: isD };

					}
*/
					unsafeWindow.mlog(list);
					unsafeWindow.load_player(list);
				},
				onerror: function(response) {
					alert(response);
				}
			});
		}, 0);
	}
};

services.youtube['page'] = /https?:\/\/www.youtube.com\/(watch|embed)/;

services.youtube['pageFn'] = function() {

	if (/https?:\/\/(www.)?youtube(.googleapis)?.com\/(watch|v|embed)/.exec(window.location)){

		if (/https?:\/\/www.youtube.com\/watch/.exec(window.location)){
			var title = $("meta[property='og:title']").attr("content");
			var img = $("meta[property='og:image']").attr("content");
			var url = window.location.href;
		}
		if (/https?:\/\/www.youtube.com\/embed/.exec(window.location)){
		
		}
		
		panel({
			btnfn:{
				name:'addPlaylistFrame',
				arg: '\''+title+'\',\''+url+'\',\''+img+'\'',
				href: 'http://'+BASE_URL+'add.html?url='+encodeURIComponent(url)
						+'&title='+title+'&img='+img,
				class: 'addbtn'
			},
			title:'Добавить в OVPlayer'
		});
		panel({
			btnfn:{
				name: 'document.location.href',
				arg: 'http://'+BASE_URL+'player.html?url='+encodeURIComponent(url),
				href: 'http://'+BASE_URL+'player.html?url='+encodeURIComponent(url),
				class: 'showbtn'
			},
			title:'Смотреть в OVPlayer'
		});
		
	};
};

services.youtube['favicon'] = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAMCAYAAACJOyb4AAAACXBIWXMAAAsSAAALEgHS3X78AAABhElEQVR4nKWSTShEURSA7/BsZYGyEGUlrG0oKc2UlSiEspGFJEtRNjaUv1IKG8XCT0pSk7DBQkqSspKFoiazIRJmnu/0TuPd26y49c27754z3z2ddzzfRD+NMZcwCfvGj8v731YkZrk8fvKgFvrgBJ7/LHdcngpXYJ2qU/8QG9cl8jY2CSslEpMK0pn3bJf+5uRAipy06xL5OYlrPOcIJPW8GwZEC5vEt4ndq1Rk9TAIpfACXZBwXSIvhzGoI9CuNx/CNHxAJawSayT2zb4BluAAWmE+VK3lEvk7HMGjVjxD8gPBcfZV0ALF0MnZBs8JqIBC2IKpULMsl8h7oBf6YTGUuADNupee50MRXJlg3EQ0SyFPof9YLpHH4A5utcfB8uM+lb6yi0KuypqgGsr0AndsLZfIZSaT2sMdJ/lMk2u0cmnJrgk+6BAFfDn5lsvTQ0m6gVMrVT5gJDZigr53QAkUwCjsmewr4xL5NQwjOs6a6scvuOCNnbRIpkVGb1nn2l2W6we1CpjFBy/BuAAAAABJRU5ErkJggg==';