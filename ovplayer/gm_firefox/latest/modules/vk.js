/*!
	Module vkParse 1.12
	for UserSripts OVPlayer (OnlineVideoPlayer)
	@date 		2014-03-10
	@autor		BaNru
	@license	MIT License
	@other 		Если долго, долго, долго,
				Если долго по тропинке,
				Если долго по дорожке
				Топать, ехать и бежать,
				То, пожалуй, то, конечно,
				То, наверно, верно, верно,
				То, возможно, можно, можно...
*/

// редирект с https
if (/^https:\/\/vk.com/.exec(window.location)){
	location.replace('http://'+location.hostname+location.pathname+location.search);
};

if(!services){var services = new Object();};

services.vk = {

	parseFn : function(url) {

		setTimeout(function(){
			GM_xmlhttpRequest({
				method: 'GET',
				url: url,
				onload: function(response)
				{
					var str = response.responseText;

					var video_array = str.match(/http:\\\/\\\/([^"]*)(240|360|480|720)\.(mp4|flv)/g);

					// Хак на старые видео
					if (video_array === null){
						mlog("Запущен хак на старые видео",{t:'info'});
						video_array = str.match(/http:\\\/\\\/([^:"]*)\.flv/g);
					};

					// Хаки не прошли, выводим ошибку
					if(video_array === null){
						alert("Ссылка на видео не найдена!");
						return;	
					};

					for (var i = 0; i < video_array.length; i++) {
						video_array[i] = video_array[i].replace(/\\/g, '');
					};

					var img = str.match(/"thumb":"(http:\\\/.*?\.jpg)/i)[1].replace(/\\/g, '');

					document.getElementById('player').setAttribute('style','background-image: url("'+img+'")');

					mlog(img,{n:'Картинка'});
					mlog(video_array, {n:'Ссылки'});

					
					var list = new Array();
					var hd_array = {'240':'800',"360":"1600","480":"2000","720":"2400"};
					var hd = true;
					var isD = "false";
					var res = null;
					var gT = 0;
					var val = video_array.length;
					var i = 0;
					var btr = [];
					
					for (i = 0; i < val; i++) {
						btr[i] = video_array[i].match(/(240|360|480|720)(?=\.)/g);
					}
					if (btr != null){
						if ( hd === true ) { // find max
							res = Math.max.apply(Math, btr);
						} else if ( hd === false ) { // find min
							res = Math.min.apply(Math, btr);
						} else { // find number
							btr = btr.sort(function(a,b){return a-b;});
							i = 0;
							while ( btr[i] <= hd ) {
								i++;
							}
							if ( i > 0 ) {i--;}
							if ( i == btr.length ) {
								i = 0;
								while ( btr[i] < hd ) {
									i++;
								}
								i--;
							}
							res = btr[i];
						}
					}
					for (i = 0; i < val; i++) {
						if (btr != null){
							if (res == btr[i] && gT == 0){
								isD = true;
								gT++
							} else {
								isD = false;
							}
							list[i] = {url: video_array[i], bitrate: (hd_array[res]+i), label: btr[i], isDefault: isD };
						} else {
							list[i] = {url: video_array[i], label: '???'};
						}
					};
					
					/*for (var i = 0; i < video_array.length; i++) {
						var btr = video_array[i].match(/(240|360|480|720)(?=\.)/g);
						if (btr != null){
							switch (btr[0]) {
								case '240':
									var c_bitrate = '800';
									var isD = false;
									break;
								case '360':
									var c_bitrate = '1600';
									var isD = false;
									break;
								case '480':
									var c_bitrate = '2000';
									var isD = false;
									break;
								case '720':
									var c_bitrate = '2400';
									var isD = true;
									break;
								default:
									var c_bitrate = '400';
									var isD = false;
									break;
							};

							list[i] = {url: video_array[i], bitrate: c_bitrate, label: btr[0], isDefault: isD };
						} else {
							list[i] = {url: video_array[i], label: '???'};
						}
					};*/
					
					mlog(list,{t: 'table'});
					load_player(list);

					//var link_video = decodeURIComponent(unsafeWindow.parseUrl(window.location['href'])["url"]);
					//$('#link_video').attr('href', link_video);
					//var link_page_video = unsafeWindow.parseUrl(link_video);
					//$('#link_page_video').attr('href', 'http://vk.com/video'+link_page_video['oid']+'_'+link_page_video['id']);
				},
				onerror: function(response) {
					alert(response);
				}
			});
		}, 0);
	}
};

services.vk['page'] = /https?:\/\/vk.com\/video/;

services.vk['pageFn'] = function() {

	if (/https?:\/\/vk.com\/video/.exec(window.location)){
		get_postMessage();
		$('.video_row_relative').click(function(){
			document.body.innerHTML = '<h1 style="color: red;text-align:center;">Необходимо перезагрузить страницу</h1>';
			setTimeout(function(){
				location.replace(window.location);
			},1000);
		});
		if (/video-?(\d+)_(\d+)/.exec(window.location)) {
			var title = $("meta[property='og:title']").attr("content");
			var url = $("meta[property='og:video']").attr("content");
			url = url.replace(/\?act=get_swf&/,'_ext.php?');
			url = url.replace(/&vid=/,'&id=');
			url = url.replace(/embed_/,'');
			var img = $("meta[property='og:image']").attr("content");
		};

		if (/https?:\/\/vk.com\/video_ext.php/.exec(window.location)){
			var a = 0;
			if (video_title){
				var title = decodeURI(video_title).replace(/\+/g,' ');
			} else {
				var title = decodeURIComponent(document.body.innerHTML.match(/var video_title = '(.*)';/)[1]).replace(/\+/g,' ');
				++a;
			}
			if (vars){
				var img = vars['jpg'];
			} else {
				var img = 'http://'+document.body.innerHTML.match(/"jpg":"http:\\\/\\\/(.*)\.jpg",/)[1].replace(/\\/g,'')+'.jpg';
				++a;
			}
			var url   = window.location.href;
		};

		title = title;
		url = url;
		img = img;

		if (a==2){
			panel({
				go:{
					page: 'add',
					get: 'title='+encodeURIComponent(title)+'&url='+encodeURIComponent(url)+'&img='+encodeURIComponent(img),
					class: 'addbtn',
				},
				title:'Добавить в OVPlayer'
			});
		}else{
			panel({
				btnfn:{
					name:'addPlaylistFrame',
					arg: '\''+title+'\',\''+url+'\',\''+img+'\'',
					class: 'addbtn'
				},
				title:'Добавить в OVPlayer'
			});
		};

		panel({
			go:{
				page: 'player',
				get: 'url='+encodeURIComponent(url),
				class: 'showbtn',
			},
			title:'Смотреть в OVPlayer'
		});
	}

};

//services.vk['favicon'] = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAMCAYAAACNzvbFAAAACXBIWXMAAA7DAAAOwwHHb6hkAAACSklEQVR4nI1TTWgTQRSe2WzStXowUnrxIoo/l+qalh4sCvWSQz0UEURtmlZCMYcmLUY9CIL04kWTtUZFxKSthXoQAh7agyKIKGraqhcP6kHM0YAtFHc3uzN+m+7E3VTBB8m+N++97/tm5o2sDmRLFuO7CSHU+eHv86ZQ4NSbQmrt4GDuiGWzAudkJ1k3LlE6/3F2vM+NyYGBXIZxHnd7KUeNDG+Bcx7FolLvImSfYbF+uLPL02Mv1FjuGoDvuRho4m0CsDOutTHOLoC03e11TJeXpsfu7j+TPQy206LYZixzKDH5+NX9UR1hhfiM6sID2YgAFCZJNC87TlCWrps1+wSYQnVGTtQ13ToHN/dHQMNMV+UuCDnvA6R0uSUoTdRBF6fSS1D7CMcQEwVouILGBbjb/Jhc6U3e3lyzWR7kjRyO8WcoGBh+V0ityGIRaq+aln0MhWFXbRjxPMoNHyQnkeqq8RKkqmeZBSSaKRdTH5ygAQq1X9VYdsKy+Q0PwI6NuyetTYDO2JgBSXotYtmbDG9RblVX9Shbn4b/NtAq2NVM15B2vFxMf/OBPr+TrHXFtYRh2c+gcs+/QJwLAfFeR7VnVxGzxp6gv09ubihPpSudQ1o/pgEXRzqa89jq9/atSs+PFT1qMz4HlS0iB6IO02LFDaCOLRbTn7rPTvYapnURhScBvp2Io6Lky9N88he8Eu4gYdtc454JQf3Rv4I69vbBaBWfS93DN+cwPiOMkx4cXkiSSEnUvJ8ZfxgZ1FrxWC5T55Hi3YCg8hskdRPb9BvGxAAAAABJRU5ErkJggg==';
//services.vk['favicon'] = 'data:image/svg+xml-compressed;base64,H4sICO/+JlMAA3ZrLnN2ZwBNUstuE0EQvCPxD63lOhP3a16RnUg5cOMGH4ASx7G02FFsxYGvp3rNgctOTW8/qqpnff/xa6b37dtpfzxsJrnhibaHx+PT/rDbTD++f819ur/7/Gl9et8RUg+nzfRyPr/erlaXy+XmYjfHt91KmXmFjOn/ToLbfnt5OH5sJiYmETWl6iwTGtJ6F19av/48v9Dp/Hvebqbn/TzffqnW+wN4PG2mb9ZEyZs/qrdUGnmVJFqo1JbMlUQlaWFqlgrjKpy6NbKaTJm0pT4a5d5RpJWyS00iwyjLcE5ZrfOCW+AB3IHEhbJyxMSiaCxozlJLJ37MOiQhV1tL0tsVmFRiXMGlgCN0Uu+KQDcqnoSZasERJEFndHDGaToIbWvSDnmG0dBVnDorJBiSHf+aECp4AV4rhvtCucGRVlNujC7BsiOxGALFyO0fKBIVIuGdVxAYzRc0Sx0hyNWQAN+jJV9bVguXUJINovOI9iINJsLAHiZhZm4V4epIbVIwDWqyFI3fbZBiAdkgWUNX1sqgx02vcIbHUBp+wnX4CS9gA/z0UVPtaKWIFO8xk6FeSmyrwD/AAmh+3QP4Wux3QQWSOTyB0r6srmmUhbqF9NDgHIt1ENV4IIw6ZMx5GAejwp68Rip2hg3mUTjhHf6ZVsvjXcXrXcebv/sLWLQWq0MDAAA=';
services.vk['favicon'] = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMTIzMiA2NDAxIj4NCiA8Zz4NCiAgPHBhdGggc3R5bGU9ImZpbGw6IzYzODhCMCIgZD0iTTM3MTIgNDc0YzI0Nyw1NyA0NjEsMTI1IDU2NywzNDIgMTIxLDI1MCA3Myw1MDIgMTEwLDgzNyAzNiwzMjAgMjcsODk3IC04OCwxMjI2IC00MTYsMTE5MyAtMTk0MCwtMjM4MCAtMTk0NywtMjM5MCAtODcsLTE0MSAtMjA3LC0yMTMgLTQxOSwtMjEzbC0xNjU4IDBjLTI5MSwwIC0yNzcsMTg3IC0yNzcsMzE2IDAsMTgwIDc1MSwxNjQwIDg4MiwxODgzIDU0LDEwMCA2NSwxMDIgMTE4LDE5OCA3MzgsMTMyOSAxNjU2LDI4NTcgMzE0MSwzNDU0IDgwMiwzMjMgMTE0NiwyNzEgMTk4MCwyNzEgNDY2LDAgNDQ3LC0yNzcgNDc2LC03MDkgMTksLTI4MSAxNTMsLTc1MyA0MzMsLTc1MyA1MTYsMCAxMTYxLDE0NjIgMTk3NCwxNDYybDE2OTggMGM0MjMsMCA2MzgsLTI3MCA0NzYsLTYzMyAtMTYxLC0zNTggLTkxOSwtMTE3NiAtMTE5OCwtMTQwOSAtNzY5LC02NDMgLTcxNSwtNzI5IC0xNTIsLTE0NzkgMjI3LC0zMDIgMjE0MSwtMjYwMSAxMDcyLC0yNjAxbC0yMDU0IDBjLTI4NywwIC0zMjksMjg3IC00OTYsNjg5IC0yMjksNTQ4IC0xMTAwLDIxNTAgLTE1MTgsMjE1NSAtMTM0LDAgLTI3NiwtMzI2IC0yNzYsLTU1MyAwLC03MDIgMTg5LC0yMTcyIC0xNTgsLTI0MDkgLTkyLC02NCAtNDE0LC0xMjcgLTgwNiwtMTU4bC05MzAgMGMtNTA0LDQ2IC05MjUsMTgzIC05NTAsNDc0eiIvPg0KIDwvZz4NCjwvc3ZnPg==';