// ==UserScript==
// @name		OVPlayer
// @namespace	OVPlayer
// @version		1.01234567890987654321
// @include		http://ovpsrc.generalvideo.ru/*
//
//				TVZAVR
// @\include	http://www.tvzavr.ru/*
//
//				YOUTUBE
// @include		http://www.youtube.com/watch*
// @include		https://www.youtube.com/watch*
// @include		http://www.youtube.com/embed*
// @include		https://www.youtube.com/embed*
// @include		https://youtube.googleapis.com/v*
// @require		http://ovpsrc.generalvideo.ru/ovplayer/gm_firefox/latest/modules/youtube.js
//
// @grant		GM_getResourceURL
// @grant		GM_getResourceText
// @grant		GM_xmlhttpRequest
// @grant		unsafeWindow
// @grant		GM_log
// @grant		GM_info
//
// @require		http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js
// @require		http://ovpsrc.generalvideo.ru/flowplayer3/flowplayer-3.2.12.min.js
// @require		http://ovpsrc.generalvideo.ru/flowplayer3/flowplayer.playlist-3.2.10.min.js
// @require		http://ovpsrc.generalvideo.ru/ovplayer/gm_firefox/latest/OVPlayer_fn.js
// @resource	style			http://ovpsrc.generalvideo.ru/style/style.css
// @resource	panel_style		http://ovpsrc.generalvideo.ru/style/panel.css
//
//				vk
// @include		http://vk.com/video*
// @include		https://vk.com/video*
// @require		http://ovpsrc.generalvideo.ru/ovplayer/gm_firefox/latest/modules/vk.js
//
//				sibnet
// @include		http://video.sibnet.ru/*
// @include		https://video.sibnet.ru/*
// @require		http://ovpsrc.generalvideo.ru/ovplayer/gm_firefox/latest/modules/sibnet.js
//
//				my-hit
// @include		http://my-hit.org/film/*
// @include		https://my-hit.org/film/*
// @require		http://ovpsrc.generalvideo.ru/ovplayer/gm_firefox/latest/modules/my-hit.js
//
//				ivi
// @include		http://www.ivi.ru/watch/*
// @include		https://www.ivi.ru/watch/*
// @include		http://www.ivi.ru/video/*
// @include		https://www.ivi.ru/video/*
// @require		http://ovpsrc.generalvideo.ru/ovplayer/gm_firefox/latest/modules/ivi.js
//
//				myvi
// @include		http://myvi.tv/*
// @include		https://myvi.tv/*
// @include		http://myvi.ru/*
// @include		https://myvi.ru/*
// @include		http://www.myvi.ru/*
// @include		https://www.myvi.ru/*
// @require		http://ovpsrc.generalvideo.ru/ovplayer/gm_firefox/latest/modules/myvi.js
//
// ==/UserScript==
(function(){

try {

BASE_URL = 'ovpsrc.generalvideo.ru/';
unsafeWindow.BASE_URL = BASE_URL;

window.backup = new Object;
unsafeWindow.backup = backup;

function load_config(){
	//unsafeWindow.config = JSON.parse(localStorage.getItem('config'));
	config = JSON.parse(localStorage.getItem('config'));
	if (config == null){
		localStorage.setItem('config', JSON.stringify(new Object));
		config = JSON.parse(localStorage.getItem('config'));
	}
	config['version'] = GM_info.script.version;
	//unsafeWindow.config = config;
	unsafeWindow.config = cloneInto(config, unsafeWindow);
	//exportFunction(config, unsafeWindow, {defineAs: 'config'});
	
};
//unsafeWindow.load_config = load_config;
//unsafeWindow.load_config();
exportFunction(load_config, unsafeWindow, {defineAs: 'load_config'});
load_config();

console.log(config);

get_postMessage();

if(config['debug'] == 'true'){
	mlog(GM_info,{n:'Информация о скрипте',t:'info'});
}

if(!document.getElementById('ovplayer_style')){
	if (new RegExp(BASE_URL).exec(window.location)){
		$('head').append('<link rel="stylesheet" type="text/css" href="'+GM_getResourceURL("style")+'" id="ovplayer_style" />');
	}else{
		$('head').append('<link rel="stylesheet" type="text/css" href="'+GM_getResourceURL("panel_style")+'" id="ovplayer_style" />');
	};
};


allServicesLength = Object.keys(services).length;
for (var i = 0; i < allServicesLength; i++){
	services[Object.keys(services)[i]].pageFn();
};


panel({go:'player',title:'OVPlayer'});
panel({go:'playlist',title:'Плейлист'});
panel({go:'config',title:'Настройки'});
panel({go:'index',title:'Главная'});


if (new RegExp(BASE_URL+'player').exec(window.location)){

	if (typeof parseUrl(window.location['href'])["url"] == 'string'){
		upd_cur_pls = function(){
			var pls = all_playlist();
			var new_list = "";
			var c_pls = '';
			var c_img = JSON.parse(localStorage.getItem('playlist_cover'));
			if (c_img == null){c_img = '';}
			for(var i = 0; i < pls.length; i++){
				c_pls = ''; c_pls = pls[i].replace(/^pls_/,'');
				new_list += '<li onclick="showPlaylist(\''+c_pls+'\')"';
				if(c_img[c_pls]){
					new_list += ' style="background-image:url(\''+c_img[c_pls]+'\');"';
				}
				new_list += '><span>'+c_pls+'</span><small onclick="editPlaylist(\''+c_pls+'\');cancelEvent(parentNode);"></small><small onclick="localStorage.removeItem(\''+pls[i]+'\');upd_cur_pls();cancelEvent(parentNode);"></small></li>';
			}
			//jQuery //$("#playlist").html('<a id="prev">&#8249;</a><ul id="spls">'+new_list+'</ul><a id="next">&#8250;</a>');
			document.getElementById("playlist").innerHTML = '<a id="prev">&#8249;</a><ul id="spls">'+new_list+'</ul><a id="next">&#8250;</a>';
			setTimeout(function() {
				carousel();
			}, 0);
		};
		upd_cur_pls();
	} else {
		setTimeout(function() {
			update_playlist();
		}, 0);
	}
	
	select_playlist();
	panel({btnfn:{name:'playlistToggle',class:'plsbtn'},title:'Свернуть/развернуть плейлист'});
	
	if(config['version'] == 'undefined' || config['version'] != '1.01234567890987654321'){
		document.getElementById('sv').innerHTML = '<a href="http://'+BASE_URL+'ovplayer/gm_firefox/latest/OVPlayer.user.js">Доступна новая версия OVPlayer</a>';
	}
	
	return;
}

if (new RegExp(BASE_URL+'config').exec(window.location)){
	load_cfg_on_page_config();
	return;
}

if (new RegExp(BASE_URL+'add').exec(window.location)){
	return;
}

if (new RegExp(BASE_URL+'playlist').exec(window.location)){
	return;
}

if (new RegExp(BASE_URL).exec(window.location)){

	var info = [
		'<h5>Это надо знать!</h5><p>Чтобы заработал модуль просмотра видео с сервиса my-hit.org, необходимо установить расширение для FireFox <a href="https://addons.mozilla.org/firefox/downloads/latest/953/" title="Установить RefControl" class="ttr">RefControl</a> и добавить сайт ovpsrc.generalvideo.ru в список блокируемых сайтов.</p>',
		'<h5>Ваше мнение улучшит сайт!</h5><p>Мы адекватно относимся к критике и рады похвале. <a href="http://feedback.ovplayer.ru/" title="Оставить отзыв или предложение" class="ttr">Оставьте отзыв или предложение</a> об OVPlayer и мы обязательно прислушаемся к нему и улучшим функционал OVPlayer\'а</p>',
		'<h5>Расскажите о проекте близким!</h5><p>Если Вам понравился проект OVPlayer, расскажите о нём друзьям и знакомым.</p>',
		'<h5>Сортировка плейлистов</h5><p>На странице <a href="http://ovpsrc.generalvideo.ru/playlist.html" title="Перейти на страницу Управления плейлистами" class="ttr">управления плейлистами</a> можно с помощью Drag\'n\'Drop (тащи и бросай) сортировать списки вручную.</p>'
	];
	
	//console.log(info[Math.floor(Math.random() * info.length)]);
	document.body.innerHTML = '<ol id="index_menu"><li><a href="player.html">Плеер</a></li> <li><a href="playlist.html">Плейлисты</a></li> <li><a href="config.html">Настройки</a></li></ol><div class="info">'+info[Math.floor(Math.random() * info.length)]+'</div>';
	
	return;
};
/*


if (/http:\/\/www.tvzavr.ru\//.exec(window.location)){
	unsafeWindow.get_postMessage();
	if (/http:\/\/www.tvzavr.ru\/action\/window\/(\d+)/.exec(window.location)){
		var title = $('title').html().replace(/&nbsp;|Фильм| смотреть онлайн на TVzavr.ru/g,'');
		var url = window.location.href;
		var img = 'http://www.tvzavr.ru/cache/620x348/'+url.match(/(\/|=)(\d+)/)[2]+'.jpg';
	} else {
		var title = $("meta[property='og:title']").attr("content");
		var img = $("meta[property='og:image']").attr("content");
		var url = 'http://www.tvzavr.ru/action/window/'+unsafeWindow.clip_id;
	}
//
}

if (/https?:\/\/(www.)?youtube(.googleapis)?.com\/(watch|v|embed)/.exec(window.location)){

	if (/https?:\/\/www.youtube.com\/watch/.exec(window.location)){
		var title = $("meta[property='og:title']").attr("content");
		var img = $("meta[property='og:image']").attr("content");
		var url = window.location.href;
	}
	
	if (/https?:\/\/www.youtube.com\/embed/.exec(window.location)){
	
	}
	
};
*/

} catch(e) {
	setTimeout(function() {
		console.log(e);
	}, 1000);
}

})();
