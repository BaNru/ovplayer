function mlog(m,d=false){
	if(config['debug'] == 'true'){
		try{
			if(d.n){
				if(!d.t || d.t=='log'){
					console.log(d.n+':\r\n',m);
				} else if (d.t=='error'){
					console.error(d.n+':\r\n',m);
				} else if (d.t=='warn'){
					console.warn(d.n+':\r\n',m);
				} else if (d.t=='info'){
					console.info(d.n+':\r\n',m);
				} else if (d.t=='table'){
					console.table(d.n+':\r\n',m);
				}
			}else{
				if(!d.t || d.t=='log'){
					console.log(m);
				} else if (d.t=='error'){
					console.error(m);
				} else if (d.t=='warn'){
					console.warn(m);
				} else if (d.t=='info'){
					console.info(m);
				} else if (d.t=='table'){
					console.table(m);
				}
			}
		//console.log();
		//console.error();
		//console.warn();
		//console.info();
		}catch(e){
			GM_log(e+' : '+m);
		}
	}
};//unsafeWindow.mlog = mlog;
exportFunction(mlog, unsafeWindow, {defineAs: "mlog"});

/*
 *function SandBoxId(e){
	return unsafeWindow.document.getElementById(e);
};
*/

/* получение GET из URL */
function parseUrl(url){
	var vars = [], hash;
	//var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	var hashes = url.slice(url.indexOf('?') + 1).split('&');
	for(var i = 0; i < hashes.length; i++){
		hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
	};
	return unsafeWindow.vars = cloneInto(vars, unsafeWindow);
};//unsafeWindow.parseUrl = parseUrl;
exportFunction(parseUrl, unsafeWindow, {defineAs: "parseUrl"});

function cancelEvent(a){
	a.onclick = function(e) {
		e.stopPropagation();
	};
};exportFunction(cancelEvent, unsafeWindow, {defineAs: "cancelEvent"});

/* Доюбавить в плейлист */
function addPlaylistFrame(title,url,img){
	// Добавить зашиту на повторный выхов
		var w = document.body.clientWidth;
		if (w > '784'){var m = '-400px';var l = '50%';}
		else {var m = '0';var l = '0';}
		var el = document.createElement("iframe");
		document.body.appendChild(el);
		el.id = 'addPlaylistFrame';
		el.style = "width:100%; max-width:784px;height:90%;left:"+l+";top:5%;margin:0 0 0 "+m+";position:fixed;z-index: 9999;border:1px solid #666;box-shadow:1px 1px 5px #666;";
		el.src = 'http://'+BASE_URL+'add.html?title='+encodeURIComponent(title)+'&url='+encodeURIComponent(url)+'&img='+encodeURIComponent(img);
		document.body.appendChild(el);
};exportFunction(addPlaylistFrame, unsafeWindow, {defineAs: "addPlaylistFrame"});

/* Доюбавить/Переименовать в плейлист */
function renamePlaylistFrame(title,pls=false){
	//unsafeWindow.mlog(title),{t:'warn'});
	var w = document.body.clientWidth;
	if (w > '784'){var m = '-400px';var l = '50%';}
	else {var m = '0';var l = '0';}
	var el = document.createElement("iframe");
	document.body.appendChild(el);
	el.id = 'addPlaylistFrame';
	el.style = "width:100%; max-width:784px;height:90%;left:"+l+";top:5%;margin:0 0 0 "+m+";position:fixed;z-index: 9999;border:1px solid #666;box-shadow:1px 1px 5px #666;";
	el.src = 'http://'+BASE_URL+'add.html?title='+encodeURIComponent(title)+'&type=rename';
	if(pls!=false){el.src += '&pls='+pls;}
	document.body.appendChild(el);
};exportFunction(renamePlaylistFrame, unsafeWindow, {defineAs: "renamePlaylistFrame"});

/* Удалить из плейлиста */
function remove_from_playlist(a, p = config['playlist default']) {
/*
	var playlist = JSON.parse(localStorage.getItem('pls_'+unsafeWindow.config['playlist default']));
	delete playlist[a];
	localStorage.setItem('pls_'+unsafeWindow.config['playlist default'], JSON.stringify(playlist));
*/
	var playlist = JSON.parse(localStorage.getItem('pls_'+p));
	backup['pls_'+p] = playlist[a];
	delete playlist[a];
	
	var r = new Date().getTime();
	/*
	panel({
		btnfn:{
			name:[
					'localStorage.setItem',
					'pls_'+r+'.parentNode.removeChild',
					//f
			],
			arg:[
					'\'pls_'+e+'\',backup[\'pls_'+e+'\']',
					"pls_"+r,
					"\'"+e+"\'"
			],
			id:		'pls_'+r,
			class:	'trash'
		},
		title: 'Восстановить трек: &laquo;'+e+'&raquo;'
	});
	*/
	localStorage.setItem('pls_'+p, JSON.stringify(playlist));
	
	if (new RegExp(BASE_URL+'player').exec(window.location)){
		update_playlist();
	}
};
exportFunction(remove_from_playlist, unsafeWindow, {defineAs: "remove_from_playlist"});

function removePlaylist(e){
	backup['pls_'+e] = localStorage.getItem('pls_'+e);
	var r = new Date().getTime();

	if (typeof playlist_load == "function" || typeof playlist_load == "function"){f = 'playlist_load'}
	else if (typeof showlist == "function" || typeof showlist == "function"){f = 'showlist'}
	else {f = 'mlog'}

	panel({
		btnfn:{
			name:[
					'localStorage.setItem',
					'pls_'+r+'.parentNode.removeChild',
					f
			],
			arg:[
					'\'pls_'+e+'\',backup[\'pls_'+e+'\']',
					"pls_"+r,
					"\'"+e+"\'"
			],
			id:		'pls_'+r,
			class:	'trash'
		},
		title: 'Восстановить плейлист: &laquo;'+e+'&raquo;'
	});

	localStorage.removeItem('pls_'+e);
	mlog(e,{n:'Плейлист удален'});
};
exportFunction(removePlaylist, unsafeWindow, {defineAs: "removePlaylist"});

/* Редактировать плейлист */
function editPlaylist(title){
	// Добавить зашиту на повторный выхов
		var w = document.body.clientWidth;
		if (w > '784'){var m = '-400px';var l = '50%';}
		else {var m = '0';var l = '0';}
		var el = document.createElement("iframe");
		document.body.appendChild(el);
		el.id = 'addPlaylistFrame';
		el.style = "width:100%; max-width:784px;height:90%;left:"+l+";top:5%;margin:0 0 0 "+m+";position:fixed;z-index: 9999;border:1px solid #666;box-shadow:1px 1px 5px #666;";
		el.src = 'http://'+BASE_URL+'add.html?title='+encodeURIComponent(esc(title))+'&type=editpls';
		document.body.appendChild(el);
};exportFunction(editPlaylist, unsafeWindow, {defineAs: "editPlaylist"});

/* Загрузить новый плейлист */
function load_new_playlist(text){
	for (var key in text) {
		if (key == 'Playlist_info'){
			var name = text[key].playlist_title;
			
			var playlist_cover = JSON.parse(localStorage.getItem('playlist_cover'));
			if(!playlist_cover){playlist_cover = new Object();}
			playlist_cover[name] = text[key].playlist_cover;

			delete text['Playlist_info'];
		}
	};
	
	localStorage.setItem('pls_'+name, JSON.stringify(text));
	if(typeof playlist_cover != 'undefined'){localStorage.setItem('playlist_cover', JSON.stringify(playlist_cover))};
	if(name){return name}
};exportFunction(load_new_playlist, unsafeWindow, {defineAs: "load_new_playlist"});

function update_playlist() {
	try {
		var playlist = JSON.parse(localStorage.getItem('pls_'+config['playlist default']));
	} catch(e){
		//console.info('Плейлист пустой: ', e);
		mlog(e,{n:'Плейлист пустой: ',t:'info'});
		return;
	}
	var keys = Object.keys(playlist);
	var len = keys.length;
	var i, k, htmllist = '';
	//function a(a,b){return parseInt(a, 10) - parseInt(b, 10);};
	function sort_alg(a, b) {
		a = new String(a);
		b = new String(b);    
		function chunkify(t) {
			var tz = [], x = 0, y = -1, n = 0, i, j;

			while (i = (j = t.charAt(x++)).charCodeAt(0)) {
				var m = (i == 46 || (i >=48 && i <= 57));
				if (m !== n) {
					tz[++y] = "";
					n = m;
				}
				tz[y] += j;
			}
			return tz;
		};

		var aa = chunkify(a);
		var bb = chunkify(b);

		for (x = 0; aa[x] && bb[x]; x++) {
			if (aa[x] !== bb[x]) {
				var c = Number(aa[x]), d = Number(bb[x]);
				if (c == aa[x] && d == bb[x]) {
					return c - d;
				} else return (aa[x] > bb[x]) ? 1 : -1;
			};
		};
		return aa.length - bb.length;
	};
	if (config['playlist sort'] == 'true'){
		keys.sort(sort_alg);
		var sort_b = 'class="sort_active button ttr" title="Отключить сортировку плейлиста" onclick=\'update_cfg("playlist sort","false");update_playlist();\'';
	} else {
		var sort_b = 'class="button ttr" title="Включить сортировку плейлиста" onclick=\'update_cfg("playlist sort","true");update_playlist();\'';
	};
	for (i = 0; i < len; i++){
		k = keys[i];
		mlog(k);
		htmllist += '<li onclick="this_play(this, \''+playlist[k].url+'\');" title="'+k+'" style="background-image:url(\''+playlist[k].img+'\');"><span>'+k+'</span><small onclick="renamePlaylistFrame(\''+esc(k)+'\');cancelEvent(parentNode);" title="Редактировать трек" class="ttb"></small><small onclick="remove_from_playlist(\''+esc(k)+'\');cancelEvent(parentNode);" title="Удалить трек" class="ttb"></small>';

		for (var a = 0; a < allServicesLength; a++){
			if (services[Object.keys(services)[a]].page.exec(playlist[k].url)){
				if(services[Object.keys(services)[a]].name){
					htmllist += '<site>'+services[Object.keys(services)[a]].name+'</site>';
				} else if(services[Object.keys(services)[a]].favicon){
					htmllist += '<img src="'+services[Object.keys(services)[a]].favicon+'">';
				}
			}
		};

		htmllist += '</li>';
	};
/*
	var htmllist = "";
	for (var key in playlist) {
		htmllist += '<li onclick="this_play(this, \''+playlist[key].url+'\');" title="'+key+'" style="background-image:url(\''+playlist[key].img+'\');"><span>'+key+'</span><small onclick="renamePlaylistFrame(\''+key+'\');cancelEvent(parentNode);"></small><small onclick="remove_from_playlist(\''+key+'\');cancelEvent(parentNode);"></small></li>';
	};
*/
	document.getElementById("playlist").innerHTML = '<a id="prev">&#8249;</a><ul>'+htmllist+'</ul><a id="next">&#8250;</a>';
	document.getElementById('sort_b').innerHTML = '<a '+sort_b+'></a>';
	carousel();
};
exportFunction(update_playlist, unsafeWindow, {defineAs: "update_playlist"});


function esc(e){
    return e//.replace(/'/, "\\'")
			//.replace(/&#39;/, "\\'")
			//.replace(/&apos;/, "\\'")
			.replace(/&/g, "&amp;");
};
exportFunction(esc, unsafeWindow, {defineAs: "esc"});

function enChars(e){
	return e.replace(/-/g,  "&#45;")
			.replace(/_/g,  "&#33;")
			.replace(/\./g, "&#46;")
			.replace(/!/g,  "&#33;")
			.replace(/~/g,  "&#126;")
			.replace(/\*/g, "&#42;")
			.replace(/'/g,  "&#39;")
			.replace(/\(/g, "&#40;")
			.replace(/\)/g, "&#41;")
			.replace(/"/g,  "&#34;")
			.replace(/\\/g, "&#92;")
			.replace(/\//g, "&#47;")
			.replace(/:/g,  "&#058;")
};
exportFunction(enChars, unsafeWindow, {defineAs: "enChars"});

function deChars(e){
	return e.replace(/&#45;/g,  "-")
			.replace(/&#33;/g,	"_")
			.replace(/&#46;/g,	".")
			.replace(/&#33;/g,	"!")
			.replace(/&#126;/g,	"~")
			.replace(/&#42;/g,	"*")
			.replace(/&#39;/g,	"'")
			.replace(/&#40;/g,	"(")
			.replace(/&#41;/g,	")")
			.replace(/&#34;/g,  "\"")
			.replace(/&#92;/g,	"\\")
			.replace(/&#47;/g,	"/")
			.replace(/&#058;/g,	":")
};
exportFunction(deChars, unsafeWindow, {defineAs: "deChars"});

function all_playlist() {
	var list = [];
	var lsl = localStorage;
	for(var i = 0; i < lsl.length; i++){
		var ls = lsl.key(i);
		if(/^pls_/.test(ls)){
			list.push(ls);
		};
	};
	return unsafeWindow.list = cloneInto(list, unsafeWindow);
};
exportFunction(all_playlist, unsafeWindow, {defineAs: "all_playlist"});

function getVideo(arg){
	for (i = 0; i < allServicesLength; i++){
		if (services[Object.keys(services)[i]].page.exec(arg.url)){
			services[Object.keys(services)[i]].parseFn(arg.url);
		}
	}
/* VK *\/	if (/https?:\/\/vk.com\/video/.exec(arg.url)){
				//vkParse(arg.url);
				services.vk.fn_parse(arg.url);
/* IVI *\/	} else if (/https?:\/\/www.ivi.ru\/(watch|video)(.*)?(\/|=)(\d+)/.exec(arg.url)) {
				var videoId = arg.url.match(/(\/|=)(\d+)/)[2];
				var url = 'http://api.digitalaccess.ru/api/json/?r='+(Math.random() * 1000);
				iviParse(url,videoId);
/*tvzavr*\/	}else if (/http:\/\/www.tvzavr.ru\/action\/window\/(\d+)/.exec(arg.url)) {
				tvzavrParse(arg.url);
/*youtube*\/	}else if (/https?:\/\/www.youtube.com\/(watch|embed)/.exec(arg.url)) {
				youtubeParse(arg.url);
/*sibnet*\/	}else if (/https?:\/\/video.sibnet.ru\/shell.php/.exec(arg.url)) {
				sibnetParse(arg.url);
			};
*/
};
exportFunction(getVideo, unsafeWindow, {defineAs: 'getVideo'});

function play_p(c_url, c_title) {
	try {
		alert($f('player').getVersion());
		$f('player').addClip({url: c_url, title: c_title}, 0);
		return;
	} catch (e) {
		//console.log(e);
		mlog(e);
	}
};
exportFunction(play_p, unsafeWindow, {defineAs: "play_p"});

/*unsafeWindow.play_playlist = function(a){
	if (a = 'start'){
		//$('#playlist ul li').length;
		var c = $('#playlist ul li a small').first();
		unsafeWindow.getVideo(c.html().replace(/&amp;/g, '&'));
		//alert(c.html().replace(/&amp;/g, '&'));
		c.addClass('play');
	} else {
		unsafeWindow.getVideo($('#playlist ul li a.play small').first().html());
	}
};*/


/* Функции плейлиста */
function this_play(a,clip){
	if (a.getAttribute('class') == "pause"){
		a.setAttribute('class','play');
		p_event('resume');
	} else if (a.getAttribute('class') == "play") {
		a.setAttribute('class','pause');
		//$f().resume();
		p_event('pause');
	} else {
		$('li.play').removeClass('play');
		$('li.play').removeClass('pause');
		getVideo({url:clip});
		a.setAttribute('class','play');	
		//unsafeWindow.replaceTitle(document.getElementsByClassName('play')[0].getElementsByTagName('span')[0].innerHTML);
		replaceTitle(a.getElementsByTagName('span')[0].innerHTML);
	}
};
exportFunction(this_play, unsafeWindow, {defineAs: "this_play"});

function send_postMessage(post) {
	try {
		window.top.postMessage(post, "*");
		//console.log('postMessage send: ' + post + ' ::: ' + window.location);
		mlog(post+' ::: '+window.location,{n:'postMessage send'});
	} catch (e) {
		//console.log('postMessage error: ' + e + ' ::: ' + window.location);
		mlog(e+' ::: '+window.location,{n:'postMessage error'});
	}
};
exportFunction(send_postMessage, unsafeWindow, {defineAs: "send_postMessage"});

function get_postMessage() {
	try {
		window.addEventListener("message", function(e){
			//console.log('postMessage get: ' + e.data + ' ::: ' + window.location);
			mlog(e.data+' ::: '+window.location,{n:'postMessage get'});
			if (e.data == 'closeFrame'){
				element = document.getElementById('addPlaylistFrame');
				if(element){element.parentNode.removeChild(element);}
				if (new RegExp(BASE_URL+'player.html$').exec(window.location)){
					update_playlist();
				}
				if (new RegExp(BASE_URL+'player.html\?').exec(window.location)){
					upd_cur_pls();
				}
				if (new RegExp(BASE_URL+'playlist.html').exec(window.location)){
					playlist_load();
				}
			}
		}, false);
	} catch (e) {
			//console.log('postMessage error: ' + e + ' ::: ' + window.location);
			mlog(e+' ::: '+window.location,{n:'postMessage error'});
	}
};
exportFunction(get_postMessage, unsafeWindow, {defineAs: "get_postMessage"});

function toBase64(url, id_img) {
	setTimeout(function() {
		GM_xmlhttpRequest({
			method: "GET",
			url: url,
			binary: true,
			headers: {
				"User-Agent": "Mozilla/5.0",
			},
			overrideMimeType: 'text/plain; charset=x-user-defined',
			onload: function(response) {
				document.getElementById(id_img).src = 'data:image/jpeg;base64,' + customBase64Encode(response.responseText);
			},
			onerror: function(response) {
				alert(response);
			}
		});
	}, 0);
};
exportFunction(toBase64, unsafeWindow, {defineAs: "toBase64"});

// customBase64Encode  http://stackoverflow.com/questions/8778863/downloading-an-image-using-xmlhttprequest-in-a-userscript/8781262
function customBase64Encode(a){var b=3,enCharLen=4,inpLen=a.length,inx=0,jnx,keyStr="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"+"0123456789+/=",output="",paddingBytes=0;var c=new Array(b),encodedCharIndexes=new Array(enCharLen);while(inx<inpLen){for(jnx=0;jnx<b;++jnx){if(inx<inpLen)c[jnx]=a.charCodeAt(inx++)&0xff;else c[jnx]=0}encodedCharIndexes[0]=c[0]>>2;encodedCharIndexes[1]=((c[0]&0x3)<<4)|(c[1]>>4);encodedCharIndexes[2]=((c[1]&0x0f)<<2)|(c[2]>>6);encodedCharIndexes[3]=c[2]&0x3f;paddingBytes=inx-(inpLen-1);switch(paddingBytes){case 1:encodedCharIndexes[3]=64;break;case 2:encodedCharIndexes[3]=64;encodedCharIndexes[2]=64;break;default:break}for(jnx=0;jnx<enCharLen;++jnx)output+=keyStr.charAt(encodedCharIndexes[jnx])}return output};

var Base64 = {_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(a){var b="";var c,chr2,chr3,enc1,enc2,enc3,enc4;var i=0;a=Base64._utf8_encode(a);while(i<a.length){c=a.charCodeAt(i++);chr2=a.charCodeAt(i++);chr3=a.charCodeAt(i++);enc1=c>>2;enc2=((c&3)<<4)|(chr2>>4);enc3=((chr2&15)<<2)|(chr3>>6);enc4=chr3&63;if(isNaN(chr2)){enc3=enc4=64}else if(isNaN(chr3)){enc4=64}b=b+this._keyStr.charAt(enc1)+this._keyStr.charAt(enc2)+this._keyStr.charAt(enc3)+this._keyStr.charAt(enc4)}return b},decode:function(a){var b="";var c,chr2,chr3;var d,enc2,enc3,enc4;var i=0;a=a.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(i<a.length){d=this._keyStr.indexOf(a.charAt(i++));enc2=this._keyStr.indexOf(a.charAt(i++));enc3=this._keyStr.indexOf(a.charAt(i++));enc4=this._keyStr.indexOf(a.charAt(i++));c=(d<<2)|(enc2>>4);chr2=((enc2&15)<<4)|(enc3>>2);chr3=((enc3&3)<<6)|enc4;b=b+String.fromCharCode(c);if(enc3!=64){b=b+String.fromCharCode(chr2)}if(enc4!=64){b=b+String.fromCharCode(chr3)}}b=Base64._utf8_decode(b);return b},_utf8_encode:function(a){a=a.replace(/\r\n/g,"\n");var b="";for(var n=0;n<a.length;n++){var c=a.charCodeAt(n);if(c<128){b+=String.fromCharCode(c)}else if((c>127)&&(c<2048)){b+=String.fromCharCode((c>>6)|192);b+=String.fromCharCode((c&63)|128)}else{b+=String.fromCharCode((c>>12)|224);b+=String.fromCharCode(((c>>6)&63)|128);b+=String.fromCharCode((c&63)|128)}}return b},_utf8_decode:function(a){var b="";var i=0;var c=c1=c2=0;while(i<a.length){c=a.charCodeAt(i);if(c<128){b+=String.fromCharCode(c);i++}else if((c>191)&&(c<224)){c2=a.charCodeAt(i+1);b+=String.fromCharCode(((c&31)<<6)|(c2&63));i+=2}else{c2=a.charCodeAt(i+1);c3=a.charCodeAt(i+2);b+=String.fromCharCode(((c&15)<<12)|((c2&63)<<6)|(c3&63));i+=3}}return b}};
//unsafeWindow.Base64 = cloneInto(Base64, unsafeWindow);

/*

Оказывается (я в это всегда верил) Фокс и Хром имеют встроенный декодеровщик base64 - это atob

var base64decode = atob;
var data = base64decode('0J/RgNC40LLQtdGCIFdvcmxk');

btoa() and atob() work in some browsers:

var enc = btoa("this is some text");
alert(enc);
alert(atob(enc));

Но блять, баг - не работает с кирилицой

*/

/* работа с конфигом */
function update_cfg(c_key,c_value,t=false) {
	if (new RegExp(BASE_URL+'config').exec(window.location) && t == 'boolean'){
		if (c_value=='true'){
			c_value='false';
		} else {
			c_value='true';
		};
	} else if (new RegExp(BASE_URL+'config').exec(window.location) && t==false){
			c_value = prompt("Укажите свои параметры для "+c_key+": ", c_value);
		if (!c_value || c_value === '' || c_value === ' '){
		   return;
		};
	};
	if (typeof localStorage.getItem('config') == 'string') { 
		var config = JSON.parse(localStorage.getItem('config'));
	} else {
		var config = new Object();
	};
	config[c_key] = c_value;
	localStorage.setItem('config', JSON.stringify(config));
	load_config();
	if (new RegExp(BASE_URL+'config').exec(window.location)){
		load_cfg_on_page_config();
	};
};
exportFunction(update_cfg, unsafeWindow, {defineAs: "update_cfg"});

function load_cfg_on_page_config(){
	//var config = JSON.parse(localStorage.getItem('config'));
	if(document.getElementsByTagName('h1')[0]){
		document.body.removeChild(document.getElementsByTagName('h1')[0]);
	};
	if(document.getElementsByTagName('table')[0]){
		document.body.removeChild(document.getElementsByTagName('table')[0]);
	};
	if(document.getElementsByTagName('h1')[0]){
		document.body.removeChild(document.getElementsByTagName('h1')[0]);
	};
	if(document.getElementsByTagName('table')[0]){
		document.body.removeChild(document.getElementsByTagName('table')[0]);
	};
	
	var htmllist = "";
	for (var key in config) {
		//htmllist += '<dt><a href="'+playlist[key]+'" target="_top">'+key+'</a><a class="delete" onclick="remove_from_playlist(\''+key+'\')">-</a></dt><dd>'+playlist[key]+'</dd>';
		htmllist += '<tr><td>'+key+'</td><td><a onclick="update_cfg(\''+key+'\', \''+config[key]+"'";
		if(config[key] == 'true' || config[key] == 'false'){
			htmllist += ",'boolean'";
		}
		htmllist += ');">'+config[key]+'</a></td></tr>';
	};
	var h1 = document.createElement("h1");
	h1.innerHTML = "Настройки";
	document.body.appendChild(h1);
	var table = document.createElement("table");
	table.className = 'table_config';
	table.innerHTML = htmllist;
	document.body.appendChild(table);
	
	var h1_p = document.createElement("h1");
	h1_p.innerHTML = "Плейлисты";
	document.body.appendChild(h1_p);
	//Execute this snippet in Chrome console
	var pls_size = '';
	var pls_byte_size = 0;
	var pls_total_size = 0;
	var i = a = 0;
	for(var x in localStorage){
		pls_size += '<tr'
		if(x == 'config' || x == 'playlist_cover'){
			pls_size += ' style="background-color:rgba(0, 255, 255, 0.05);"';
			a++;
		}
		pls_size += '><td>';
		pls_size += '<span title="'+x+'">'+x.replace('pls_','');
		pls_size += "</span></td><td style='white-space:nowrap'>";
		pls_byte_size = localStorage[x].length * 2;
		pls_total_size += pls_byte_size;
		pls_size += '<span title="'+(pls_byte_size)+' байт">'
		pls_size += (pls_byte_size/1024/1024).toFixed(1);
		pls_size += " MB</span></td></tr>";
		i++
	}
	pls_size += '<tr><td>'+(i-a)+' (+'+a+')</td><td>'+(pls_total_size/1024/1024).toFixed(1)+' MB</td></tr>';
	var table_p = document.createElement("table");
	table_p.className = 'table_config';
	table_p.innerHTML = pls_size;
	document.body.appendChild(table_p);
	
	/*
	//or add this text in the field 'location' of a bookmark for convenient usage
	var x,log=[],total=0;
	for (x in localStorage){
		log.push( x + " = "
		+  ((localStorage[x].length * 2)/1024/1024).toFixed(2) + " MB");
		total+=localStorage[x].length * 2;
	};
	
	log.push("Total = " + (total/1024/1024).toFixed(2)+ " MB");
	alert(log.join("\n"));
	*/
};
exportFunction(load_cfg_on_page_config, unsafeWindow, {defineAs: "load_cfg_on_page_config"});

/*unsafeWindow.go = function(e){
	location.replace('http://ovpsrc.generalvideo.ru/'+[e]+'.html');
};*/

function playlistToggle(){
	if($('#playlist').is(":hidden")){
		$('section').animate({"height": "-=85px"}, "fast");
		$('.plsbtn').removeClass('hide');
	}else{
		$('section').animate({"height": "+=85px"}, "fast");
		$('.plsbtn').addClass('hide');
	}
	$('#playlist').slideToggle('fast');
};
exportFunction(playlistToggle, unsafeWindow, {defineAs: 'playlistToggle'});

function panel(arg){
	
	if(!document.getElementById('player_panel')){
		try {
			var c = '';
			if(config['panel_visibility'] == 'false'){
				c = 'hide';
			};
		} catch(e) {
			mlog(e,{n:'panel_visibility'});
			var c = 'false';
		};
		if (new RegExp(BASE_URL).exec(window.location)){
			$('body').append('<ul id="player_panel" class="vspp"></ul>');
		} else {
			$('body').append('<ul id="player_panel" class="'+c+'"><li><a id="pane_show_hide" onclick="panel_visibility()"></a></li></ul>');
		}
		//setTimeout(function() {
		//	unsafeWindow.panel_visibility();
		//}, 1000);
	};

	if(arg.go){
		if(typeof arg.go == 'string'){
			var append_text = '<li><a href="'+'http://'+BASE_URL+arg.go+'.html" class="button';
			if(arg.title){if(arg.title){append_text += ' tt" title="'+arg.title;}}
			append_text += '" id="'+arg.go+'-pl"></a></li>';
		}else if (typeof arg.go == 'object'){
			if(!arg.go.page){return};
			if(!arg.go.get){arg.go.get = ''}else{arg.go.get = '?'+arg.go.get};
			if(!arg.go.class){arg.go.class = ''};
			if(!arg.go.id){arg.go.id = ''}else{arg.go.id = ' id="'+arg.go.id+'"'};
			var append_text = '<li><a href="'+'http://'+BASE_URL+arg.go.page+'.html'+arg.go.get+'" class="button '+arg.go.class;
			if(arg.title){append_text += ' tt" title="'+arg.title}
			append_text += '"></a></li>';
		}
		//$('#player_panel').append(append_text);
		/*var setIntervalPP = setInterval(function(){
		if(document.getElementById('player_panel')){
			document.getElementById('player_panel').appendChild(append_text);
			clearInterval(setIntervalPP);
		}
		}, 200);*/
		//document.getElementById('player_panel').innerHTML = document.getElementById('player_panel').innerHTML + append_text;
		/*
		 	<!-- beforebegin -->
			<p>
			<!-- afterbegin -->
			foo
			<!-- beforeend -->
			</p>
			<!-- afterend -->
		*/
		document.getElementById('player_panel').insertAdjacentHTML('beforeend', append_text);
	};


	if(arg.btnfn){
		if(!arg.btnfn.class){arg.btnfn.class = '';}
		if(!arg.btnfn.arg){arg.btnfn.arg = '';}
		var append_text = '<li><a onclick="';
		if(typeof arg.btnfn.name == "object"){
			var l = arg.btnfn.name.length;
			for(var i=0; i<l; i++){
				append_text += arg.btnfn.name[i]+'('+arg.btnfn.arg[i]+');';
			}
		} else if(typeof arg.btnfn.name == "string"){
			append_text += arg.btnfn.name+'('+arg.btnfn.arg+');';
		}
		append_text += 'return false;" class="button ';
		if(arg.btnfn.class){append_text += ' '+arg.btnfn.class;}
		if(arg.title){append_text += ' tt" title="'+arg.title;}
		append_text += '"';
		
		if(arg.btnfn.id){append_text += ' id="'+arg.btnfn.id+'"'};
		
		if(arg.btnfn.href){append_text += ' href="'+arg.btnfn.href+'"'};
		
		append_text += '></a></li>';
		//$('#player_panel').append(append_text);
		document.getElementById('player_panel').insertAdjacentHTML('beforeend', append_text);
	};
};
exportFunction(panel, unsafeWindow, {defineAs: 'panel'});

function panel_visibility(){
	if($('#player_panel').attr("class") == 'hide'){
		$('#player_panel').removeClass('hide');//.animate({'right': '0'}, 'fast');
		update_cfg('panel_visibility','true', true);
	} else {
		$('#player_panel').addClass('hide');//.animate({'right': -$('#player_panel').width()+5}, 'fast');
		update_cfg('panel_visibility','false', true);
	}
};exportFunction(panel_visibility, unsafeWindow, {defineAs: 'panel_visibility'});

function showPlaylist(e){
	update_cfg('playlist default',e);
	//location.replace('http://ovpsrc.generalvideo.ru/player.html');
	update_playlist();
	history.pushState(null, null, "player.html");
	replaceTitle(false);
};exportFunction(showPlaylist, unsafeWindow, {defineAs: 'showPlaylist'});

/* Карусель плейлиста */
function carousel(){
/* этот код помечает картинки, для удобства разработки 
	var lis = document.getElementsByTagName('li');
	for(var i=0; i<lis.length; i++) {
		lis[i].style.position='relative';
		var span = document.createElement('span');
		// обычно лучше использовать CSS-классы,
		// но этот код - для удобства разработки, так что не будем трогать стили
		span.style.cssText='position:absolute;left:0;top:0';
		span.innerHTML = i+1;
		lis[i].appendChild(span);
	}
*/

/* конфигурация */
	var width = 146; // ширина изображения
	var count = Math.floor(document.body.clientWidth / width); // количество изображений
	
	//document.getElementById('playlist').style.width = width*count+64 + 'px';
	var d = (document.body.clientWidth - width*count)/2;

	document.getElementById('playlist').getElementsByTagName('ul')[0].style.padding = '0 ' + d + 'px';
	document.getElementById('prev').style.width = d  + 'px';
	document.getElementById('next').style.width = d  + 'px';
	var ul = document.getElementById('playlist').getElementsByTagName('ul')[0];
	var imgsLength = ul.getElementsByTagName('li').length;
	document.getElementById('prev').className = 'd';
	if (count >= imgsLength){
		document.getElementById('next').className = 'd';
	};
	var position = 0; // текущий сдвиг влево

	function prev_carousel(count_){
		
		if (position >= 0) { // уже до упора
			document.getElementById('prev').className = 'd';
			return false;
		};
		// последнее передвижение влево может быть не на 3, а на 2 или 1 элемент
		position = Math.min(position + width*count_, 0);
		ul.style.marginLeft = position + 'px';
		if(position == 0){
			document.getElementById('prev').className = 'd';
		} else {
			document.getElementById("next").className = "";
		};
		return false;
	};
	function next_carousel(count_){
		if (position <= -width*(imgsLength-count)){ // уже до упора
			document.getElementById('next').className = 'd';
			return false;
		};
		// последнее передвижение вправо может быть не на 3, а на 2 или 1 элемент
		position = Math.max(position-width*count_, -width*(imgsLength-count));
		//position = Math.max(position-width*count_, -width*(imgsLength-count_)+(document.body.clientWidth-(count_*width)-64));
		ul.style.marginLeft = position + 'px';
		if(position == -width*(imgsLength-count)){
			document.getElementById('next').className = 'd';
		}else{
			document.getElementById("prev").className = "";
		};
		return false;
	};
	
	document.getElementById('prev').onclick = function() {
		prev_carousel(count);
	};
	document.getElementById('next').onclick = function() {
		next_carousel(count);
	};
	
	
	/** Initialization code. 
	 * If you use your own event management code, change it as required.
	 */
	//if (window.addEventListener)
	if (document.getElementById('playlist').addEventListener){
		/** DOMMouseScroll is for mozilla. */
		//window.addEventListener('DOMMouseScroll', wheel, false);
		document.getElementById('playlist').addEventListener('DOMMouseScroll', wheel, false);}
	/** IE/Opera. */
	window.onmousewheel = document.onmousewheel = wheel;
	
	/** This is high-level function.
	 * It must react to delta being more/less than zero.
	 */
	function handle(delta) {
		if (delta < 0){
			next_carousel(1);
		} else {
			prev_carousel(1);
		};
	};

	/** Event handler for mouse wheel event.
	 */
	function wheel(event){
			var delta = 0;
			if (!event) /* For IE. */
					event = window.event;
			if (event.wheelDelta) { /* IE/Opera. */
					delta = event.wheelDelta/120;
			} else if (event.detail) { /** Mozilla case. */
					/** In Mozilla, sign of delta is different than in IE.
					 * Also, delta is multiple of 3.
					 */
					delta = -event.detail/3;
			}
			/** If delta is nonzero, handle it.
			 * Basically, delta is now positive if wheel was scrolled up,
			 * and negative, if wheel was scrolled down.
			 */
			if (delta)
					handle(delta);
			/** Prevent default actions caused by mouse wheel.
			 * That might be ugly, but we handle scrolls somehow
			 * anyway, so don't bother here..
			 */
			if (event.preventDefault)
					event.preventDefault();
		event.returnValue = false;
	}	

};
exportFunction(carousel, unsafeWindow, {defineAs: 'carousel'});

// список плейлистов
function select_playlist(){
	var list = all_playlist();
	var list_d = config['playlist default'];
	var new_list = "";
	for(var i = 0; i < list.length; i++){
		new_list += '<option';
		if (list[i].replace(/^pls_/,'') == list_d){
			new_list += ' selected';
		}
		new_list += '>'+list[i].replace(/^pls_/,'')+'</option>';
	}
	document.getElementById('select_playlist').innerHTML = '<select onchange="showPlaylist(this.value)">'+new_list+'</select>';
};
exportFunction(select_playlist, unsafeWindow, {defineAs: 'select_playlist'});

function replaceTitle(e){
	var title;
	if (e != false){title = e + ' : ';} else {title='';}
	title += config['playlist default'] + ' : ';
	document.title = deChars(title) + 'OVPlayer';
};
exportFunction(replaceTitle, unsafeWindow, {defineAs: 'replaceTitle'});

//window.onload = function(){
//}
