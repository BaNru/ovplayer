<?php

require_once $_SERVER["DOCUMENT_ROOT"].'/base/functions.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/base/db.php';

$GLOBAL_HTML = array();
$GLOBAL_HTML['body'] = '';
$GLOBAL_HTML['head'] = '';
$GLOBAL_HTML['title'] = 'Нет названия страницы';

if(db_connect($global_db) === true) {

	$get_mod = (!empty($_GET['mod'])) ? min_text($_GET['mod'], 10) : '';
	
	// Массив жанров фильмов
	$array_zhanr = array(
		1 => array('name' => 'Боевик'),
		2 => array('name' => 'Комедия'),
		3 => array('name' => 'Эротика')
	);
	
	####################
	# Добавление записей
	####################
	if($get_mod == 'add') {
		
		$min_simv_name = 5;
		$max_simv_name = 30;
		
		$min_simv_url = 5;
		$max_simv_url = 100;
		
		// Нажата кнопка добавить
		if(isset($_POST['go'])) {
			
			$true_insert = false;
			$error = array();
			$name_f = '';
			
			if(!empty($_SESSION['hash']) && $_SESSION['hash'] == $_GET['hash']) {
			
				// Название
				if(!empty($_POST['name'])) {
				
					if(mb_strlen($_POST['name']) >= $min_simv_name && mb_strlen($_POST['name']) <= $max_simv_name) {
						$name_f = min_text($_POST['name'], $max_simv_name);
					}
					else {
						$error[] = 'Число символов в названиии должно колебаться от '.$min_simv_name.' до '.$max_simv_name;
					}
					
				}
				else {
					$error[] = 'Название должно быть заполнено';
				}
				
				// Жанр
				$post_zhanr = (!empty($_POST['genre'])) ? intval($_POST['genre']) : 0;
				if(!array_key_exists($post_zhanr, $array_zhanr)) $error[] = 'Раздел выбран неверно';
				
				// Ссылка на источник
				if(!empty($_POST['url'])) {
					
					if(mb_strlen($_POST['url']) >= $min_simv_name && mb_strlen($_POST['url']) <= $max_simv_name) {
						$href = min_text($_POST['url'], $max_simv_url);
					}
					else {
						$error[] = 'Число символов в ссылке на источник должно колебаться от '.$min_simv_url.' до '.$max_simv_url;
					}
					
				}
				else {
					$error[] = 'Ссылка на источник должна быть заполнена';
				}
				
				// Если нет ошибок, то всё вносим в базу
				if(count($error) == 0) {
					
					select("START TRANSACTION");
					
					$id_insert_film = select("INSERT INTO `info` (`name`) VALUES('$name_f')", 'insert_id');
					
					if($id_insert_film > 0) {
					
						$id_insert_url = select("INSERT INTO `url` (`url`, `id_info`) VALUES('$href', $id_insert_film)", 'insert_id');
						
						if($id_insert_url > 0) {
						
							$id_file_zhanr = select("INSERT INTO `genre`(`genre`, `id_info`) VALUES($post_zhanr, $id_insert_film)", 'insert_id');
							
							if($id_file_zhanr > 0) {
								$true_insert = true;
							}
						}
					}
					
					// Все запросы прошли успешно
					if($true_insert === true) {
					
						$_SESSION['hash'] = false;
						select("COMMIT");
						
						$GLOBAL_HTML['body'] .= '<div>Фильм '.$name_f.' успешно добавлен!</div>';
					}
					else {
						select("ROLLBACK");
						$error[] = 'При занесении в базу возникла неизвестная ошибка';
					}
				}
				
			}
			else {
				$error[] = 'Ошибка двойного нажатия, фильм был уже добавлен';
			}
			if(count($error) > 0) $GLOBAL_HTML['body'] .= '<div>'.implode('<br/>', $error).'</div>';
		}
		
		$ob_zhanr = '';
		
		foreach($array_zhanr as $key => $value)  $ob_zhanr .= '<option value="'.$key.'">'.$value['name'].'</option>';
		
		$GLOBAL_HTML['title'] = 'Добавление записей';
		if(empty($_SESSION['hash'])) $_SESSION['hash'] = mt_rand(1,99999);
		
		$GLOBAL_HTML['body'] .= '
			<div>
				<form action="index.php?mod='.$get_mod.'&amp;hash='.$_SESSION['hash'].'" method="post">
					Название:
					<br/>
					<input type="text" name="name"/>
					<br/>
					<br/>
					Жанр:<br/>
					<select name="genre"><option value="0">Жанр не выбран</option>'.$ob_zhanr.'</select>
					<br/>
					<br/>
					Ссылка на источник:
					<br/>
					<input type="text" name="url"/>
					<br/>
					<br/>
					<input type="submit" name="go" value="Добавить фильм"/>
				</form>
			</div>
		';
		$GLOBAL_HTML['body'] .= '<br/><a href="/index.php">На главную</a>';
	}
	
	######################
	# Просмотр записей
	######################
	elseif($get_mod == 'sm') {
		
		$GLOBAL_HTML['title'] = 'Просмотр записей';
		
		$count_html = 2;
		
		$page = (!empty($_GET['page'])) ? intval($_GET['page']) : 0;
		if($page < 0) $page = 0;
		$key_id = $page * $count_html;
		$limit = $count_html + 1;
		
		$res = select("
			SELECT `i`.`name`, `u`.`url`, `z`.`genre`
			FROM `info` AS `i`
			JOIN `url` AS `u` ON `i`.`id` = `u`.`id_info`
			JOIN `genre` AS `z` ON `i`.`id` = `z`.`id_info`
			WHERE `i`.`id` > $key_id LIMIT $limit
			", 'array'
		);
		
		$count_res = is_array($res) ? count($res) : 0;
		
		if($count_res > 0) {
		
			$i = 1;
			
			foreach($res as $value) {
				
				$GLOBAL_HTML['body'] .= 'Название фильма: '.$value['name_film'].'<br/>';
				$GLOBAL_HTML['body'] .= 'Жанр: '.$array_zhanr[$value['zhanr']]['name'].'<br/>';
				$GLOBAL_HTML['body'] .= 'Источник: <a href="'.$value['url'].'">'.$value['url'].'</a><br/><br/>';
				
				if($i >= $count_html) break;
				$i++;
			}
			
			$GLOBAL_HTML['body'] .= ($page > 0) ? '<a href="index.php?mod='.$get_mod.'&amp;page='.($page - 1).'">Назад</a>' : 'Назад';
			$GLOBAL_HTML['body'] .= ' --- ';
			$GLOBAL_HTML['body'] .= ($count_res > $count_html) ? '<a href="index.php?mod='.$get_mod.'&amp;page='.($page + 1).'">Вперед</a>' : 'Вперед';
			
		}
		else {
			$GLOBAL_HTML['body'] .= 'Записи не найдены на это странице';
		}
		$GLOBAL_HTML['body'] .= '<br/><a href="index.php">На главную</a>';
	}
	
	###################
	# Главная страница
	###################
	else {
		$GLOBAL_HTML['title'] = 'Главная';
		$GLOBAL_HTML['body'] .= '
		<a href="index.php?mod=add">Добавить</a>
		<br/>
		<a href="index.php?mod=sm">Просмотр</a>
		';
	}
}
else {
	$GLOBAL_HTML['title'] = 'Ошибка соединения';
	$GLOBAL_HTML['body'] .= 'Не получилось соединиться с бд';
}
echo gen_html($GLOBAL_HTML);