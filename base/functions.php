<?php
session_start();
setlocale(LC_CTYPE,'ru_RU.UTF-8');
mb_internal_encoding("UTF-8");
mb_regex_encoding("UTF-8");

function db_connect($global_db) {
	$port = ($global_db['db_port'] > 0) ? ':'.$global_db['db_port'] : false;
	$connect = mysql_connect($global_db['db_host'].$port, $global_db['db_user'], $global_db['db_pass']);
	$db = mysql_select_db($global_db['db_name']);
	if($connect && $db) {
		mysql_query("SET NAMES 'utf8';");
		mysql_query("SET CHARACTER SET 'utf8';");
		mysql_query("SET SESSION collation_connection = 'utf8_bin';");
		return true;
	}
	return false;
}

function select ($query, $param = false) {

	$sql = mysql_query($query);
	if(!$sql) return false;
	
	if($param == 'rows') return mysql_num_rows($sql);
	elseif($param == 'fetch')  return (mysql_num_rows($sql) == 1) ? mysql_fetch_assoc($sql) : false;
	elseif($param == 'result') return (mysql_num_rows($sql) == 1) ? mysql_result($sql, 0) : false;
	elseif($param == 'insert_id') return mysql_insert_id();
	elseif($param == 'affected') return mysql_affected_rows();
	elseif($param == 'array') {
		$array = array();
		while($value = mysql_fetch_assoc($sql)) $array[] = $value;
		return $array;
	}
	else return false;
	
}

function min_text($min_text,$simv) {

	$tbl = array('\\'=>'');
	$min_text = strtr($min_text,$tbl);
	$min_text = mb_substr($min_text,0,$simv);
	$min_text = htmlspecialchars($min_text, ENT_QUOTES, 'utf-8');
	$min_text = trim($min_text);
	
	return $min_text;
}

function gen_html($GLOBAL_HTML) {

	return '
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru">

	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
		<title>
			'.$GLOBAL_HTML['title'].'
		</title>
		'.$GLOBAL_HTML['head'].'
	</head>
	
	<body>
		'.$GLOBAL_HTML['body'].'
	</body>
</html>';

}