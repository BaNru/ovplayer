//window.addEventListener('DOMContentLoaded', function () {
//	console.log(window.greetme('Hello!'));
//});

//function load_player_single(c_url){
function load_player(c_url){
setTimeout(function() {

	if(config['scaling'] == 'half' || config['scaling'] == 'orig' || config['scaling'] == 'scale'){var scaling = config['scaling']}
	else{var scaling = 'fit'}

	flowplayer("player", 'http://'+BASE_URL+"flowplayer3/flowplayer.commercial-3.2.16.swf",{
		//debug:true,
		key: '#$9286c26b56cde34853f',
		w3c: true,
		cachebusting: false,
		wmode: "opaque",
/*		wmode: "transparent",*/
		clip: {
			//bitrates: unsafeWindow.getVideo('http://vk.com/video_ext.php?oid=-17438229&id=164760018&hash=59e7407358411c09'),
			bitrates: c_url,
			provider: 'pseudo',
			urlResolvers: 'brselect',
			scaling: scaling,
			//seekableOnBegin: true,
			//urlEncoding: true,
			//urlResolvers: 'bwcheck'
			/*onStart: function() {
				alert('1');
				var c = $('#playlist ul li a.play').first().attr('title');
				$('a.play').removeClass('play');
				alert(c);
			}*/
			onMetaData: function(e) {
				console.log(e.metaData);
				document.getElementById('download_video').innerHTML = (e.metaData.videosize/1024/1024).toFixed(2) + ' МБ';
			},
//			autoPlay: true,
//			autoBuffering: true,
/*
			onBeforeBegin: function() {
				if (!this.isFullscreen()) {
					console.log('1: ',+this.isFullscreen());
					setTimeout(function() {
						console.log('2: ',this.isFullscreen());
						this.toggleFullscreen();
						console.log('3: ',this.isFullscreen());
					}, 1000);
					console.log('4: ',this.isFullscreen());
				}
			},
*/
		},
		plugins: {
			controls: {
				backgroundColor: "transparent",
				backgroundGradient: "none",
				sliderColor: '#FFFFFF',
				sliderBorder: '1.5px solid rgba(160,160,160,0.7)',
				volumeSliderColor: '#FFFFFF',
				volumeBorder: '1.5px solid rgba(160,160,160,0.7)',
				timeColor: '#ffffff',
				durationColor: '#535353',
				tooltipColor: 'rgba(255, 255, 255, 0.7)',
				tooltipTextColor: '#000000',
				stop: true,
			},
			content: {
				url: 'http://'+BASE_URL+"flowplayer3/flowplayer.content-3.2.8.swf",
				backgroundGradient:'none',
				backgroundColor: "transparent",
				top: 0,
				left: 0,
				width: 400,
				height: 150,
				border: 0,
				textDecoration: 'outline',
				style: {
						body: {
						fontSize: 14,
						fontFamily: 'Arial',
						textAlign: 'center',
						color: '#ffffff'
					}
				}
			},
			pseudo: {
				url: 'http://'+BASE_URL+"flowplayer3/flowplayer.pseudostreaming-3.2.12.swf"
			},
			brselect: {
				url: 'http://'+BASE_URL+"flowplayer3/flowplayer.bitrateselect-3.2.13.swf",
				menu: true,
//				events: {
//					all: true
//				},
				hdButton: {
					place: 'none',
					splash: false
				},
				onStreamSwitchBegin: function(newItem, currentItem) {
					$f().getPlugin('content').setHtml(
						"Will switch to: " + newItem.streamName +
						" from " + currentItem.streamName
					);
				},
				onStreamSwitch: function(newItem) {
					$f().getPlugin('content').setHtml(
						"Switched to: " + newItem.streamName
					);
				}
			},
			bwcheck: {
				url: 'http://'+BASE_URL+"flowplayer3/flowplayer.bwcheck-3.2.12.swf",
				//dynamic: true,
			},
			menu: {
				url: 'http://'+BASE_URL+"flowplayer3/flowplayer.menu-3.2.12.swf",
				items: [{
					label: "Качество видео",
					enabled: false
				}],
				onSelect: function(item) {
				// play the video corresponding to the selected menu item's index
					//if (item.index >= 0) {
						//$f().play(item.index);
						//alert('1');
						//console.log(item.index);// + ' | ' + this.getTime());
						console.log(item.bitrateItem);
					//}
				}
			},
		},
/*
		log : {
			level  : 'debug',
			filter : 'org.flowplayer.bitrateselect.*'
		},
*/
		canvas: {
			backgroundColor:'transparent',
			backgroundGradient:'none'
		},
		onLastSecond: function() {

			var c = $('#playlist ul li.play').first().attr('title');
			var play = JSON.parse(localStorage.getItem('pls_'+config['playlist default']));
			
			var nextKey = null;
			var firstKey = null;
			var flag = false;
			var i = 0;
			var len = Object.keys(play).length;
			for (key in play) {
				if ( i++ == 0 ) firstKey = key;
				if ( flag ) {
					nextKey = key;
					break;
				}
				if ( key == c ) flag = true;
				if ( flag && i == len ) nextKey = firstKey;
			}
			getVideo({url:play[nextKey].url});
			replaceTitle('плей');
			
//			var newplay = new Array;
//			for (var key in play) {
//				newplay.push(key);
//			}

//			getVideo(play[newplay[0]]);
/*
			var xxx = "строкаВ";

			var nextKey = null;
			var flag = false;
			for (key in obj) {
				if ( flag ) {
					nextKey = key;
					break;
				}
				if ( obj[key] == xxx ) flag = true;
			}

			alert( nextKey );
*/

			$('li.play').removeClass('play');
			$('li[title="'+nextKey+'"]').addClass('play');

		},
		/*onLoad: function(){
			if (document.getElementById('fullScreen').value == 'true'){
				console.log(this.isFullscreen());
				this.toggleFullscreen();
				console.log(this.isFullscreen());
			}
		},*/
/*
		onFullscreen: function() {
			document.getElementById('fullScreen').value = 'true';
		},
		onFullscreenExit: function() {},
*/
		play: {
			label: "Смотреть   "
		},
		/*onFinish: function() {
			var playlist = JSON.parse(localStorage.getItem('LS_playlist'));
			var nextKey = null;
			var flag = false;
			for (key in playlist) {
				if ( flag ) {
					nextKey = key;
					break;
				}
				if ( playlist[key] == document.location.href ) flag = true;
			}
			location.replace(playlist[nextKey]);
		}*/
	});//.playlist("aside#playlist", {loop:true});
}, 0);
};


function p_event(e){
	$f()[e]();
};


$(document).ready(function(){

	if (new RegExp(BASE_URL+'player').exec(window.location)){
		//var setIntervalID_1 = setInterval(function(){
			//if (typeof parseUrl(window.location['href'])["url"] == 'string'){
			if (typeof parseUrl(window.location['href']) == 'string'){
				getVideo({url:decodeURIComponent(parseUrl(window.location['href'])["url"])});
			} else {
				var play = JSON.parse(localStorage.getItem('pls_'+config['playlist default']));
				getVideo({url:play[Object.keys(play)[0]].url});
				replaceTitle(Object.keys(play)[0]);
			}
			if(config['autoplay'] == 'true' || config['autoplay'] == '1'){
				var setIntervalID_2 = setInterval(function(){
					if ($f() != null){
						$f('player').play();
						$('li[title="'+Object.keys(play)[0]+'"]').addClass('play');
						clearInterval(setIntervalID_2);
					}
				}, 200);
			}
		//}, 500);
	}

	setInterval(function(){
		$('#dt').html(new Date());
	}, 1000);

	var setIntervalID = setInterval(function(){
		if ($f() == null){
			$('#fpv').html('Плеер не загружен');
		} else {
			$('#fpv').html($f('player').getVersion());
			clearInterval(setIntervalID);
		}
	}, 1000);
});