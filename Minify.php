<?php

if ($_GET['go']!='qwerty'){
	exit;
}

require dirname(__FILE__) . '/min/config.php';

require "$min_libPath/Minify/Loader.php";
Minify_Loader::register();

// set cache path and doc root if configured
$minifyCachePath = isset($min_cachePath) 
    ? $min_cachePath 
    : '';
if ($min_documentRoot) {
    $_SERVER['DOCUMENT_ROOT'] = $min_documentRoot;
}

// default log to FirePHP
if ($min_errorLogger && true !== $min_errorLogger) { // custom logger
    Minify_Logger::setLogger($min_errorLogger);
} else {
    Minify_Logger::setLogger(FirePHP::getInstance(true));
}

error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);

header('Content-Type:text/html;charset=utf-8');

$thisDir = dirname(__FILE__);




$t = $a = $sizeOut = $sizePacked = $countFile = '';

function countBytes($str){
    return (function_exists('mb_strlen') && ((int)ini_get('mbstring.func_overload') & 2))
        ? mb_strlen($str, '8bit')
        : strlen($str);
}

function m($SrcPath, $OutPath, $type){

	global $t, $sizeOut, $sizePacked, $countFile;
	$a = '';

	$d = dir($SrcPath);
	while (false !== ($entry = $d->read())) {
		if (preg_match('/^([\w\\-\.]+)\.'.$type.'$/', $entry, $m)) {
			$list[] = '/'.$m[1].'.'.$type;
		}
	}
	$d->close();
	
	// $files = glob(dirname(__FILE__) . '/_test_files/yuic/*.css');

	foreach ($list as $item) {

		if($item == '/jquery.min.js'){continue;}
	
		$t1 = $t2 = $time = '';

		$src = file_get_contents($SrcPath.$item);
		//$minExpected = file_get_contents($thisDir . '/html/add.html');

		$t1 = microtime(true);

		if($type == 'html'){
			$minOutput = Minify_HTML::minify($src, array(
				'cssMinifier' => array('Minify_CSS', 'minify')
				,'jsMinifier' => array('JSMin', 'minify')
			));
		}elseif($type == 'css'){
		
/*function yuiCssPort($css, $options) {
    $compressor = new CSSmin();
    $css = $compressor->run($css, 9999999);
    
    $css = Minify_CSS_UriRewriter::rewrite(
        $css,
        $options['currentDir'],
        isset($options['docRoot']) ? $options['docRoot'] : $_SERVER['DOCUMENT_ROOT'],
        isset($options['symlinks']) ? $options['symlinks'] : array()
    );
    return $css;
}
$min_serveOptions['minifiers']['text/css'] = 'yuiCssPort';
		
			//$minOutput = Minify_CSS::minify($src, array());
			$options = array();
			$compressor = new CSSmin();
			$minOutput = Minify_CSS_UriRewriter::rewrite(
				$src,
				$options['currentDir'] = $thisDir . '/style',
				isset($options['docRoot']) ? $options['docRoot'] : $_SERVER['DOCUMENT_ROOT'],
				isset($options['symlinks']) ? $options['symlinks'] : array()
			);
		
*/



		// some tests may exhaust memory/stack due to string size/PCRE
		$skip = array(
			//'dataurl-base64-doublequotes.css',
			//'dataurl-base64-noquotes.css',
			//'dataurl-base64-singlequotes.css',
		);

		$cssmin = new CSSmin();

//		foreach ($list as $file) {
		if (!empty($skip) && in_array(basename($item), $skip)) {
			echo "INFO: CSSmin: skipping " . basename($item) . "\n";
			continue;
		}

		//$src = file_get_contents($SrcPath.$item);
		//$minExpected = trim(file_get_contents($item . '.min'));
		$minOutput = trim($cssmin->run($src));
		
		/*$passed = assertTrue($minExpected == $minOutput, 'CSSmin : ' . basename($item));
		if (! $passed && __FILE__ === realpath($_SERVER['SCRIPT_FILENAME'])) {
			echo "\n---Output: " .countBytes($minOutput). " bytes\n\n{$minOutput}\n\n";
			echo "---Expected: " .countBytes($minExpected). " bytes\n\n{$minExpected}\n\n";
			echo "---Source: " .countBytes($src). " bytes\n\n{$src}\n\n\n";
		}*/
//		}
		}elseif($type == 'js'){
			//$minOutput = JSMinPlus::minify($src);
			$minOutput = JSMin::minify($src);
		}


		$t2 = microtime(true);
		$time = sprintf('%.4f', ($t2 - $t1) );
		$t += $time;
		
		if ($item == '/OVPlayer.user.js'){
			preg_match("~(\/\/ ==UserScript==.*\/\/ ==/UserScript==)~isU", $src, $header);
			$minOutput = $header[0].PHP_EOL.PHP_EOL.$minOutput;
		}
		if ($item == '/player_load.js'){
			$minOutput = str_replace('$9286c26b56cde34853f','$b14c168a23df82626a5',$minOutput);
		}
		
		// Позже удалить player.html
		if ($item == '/player.html' || $item == '/OVPlayer.user.js' ){
			$minOutput = str_replace('1.01234567890987654321','1.'.date("yz"),$minOutput);
		}
		
		// Слайдер фикс
		if ($item == '/index.css'){
			$minOutput = str_replace('keyframes slider{0{','keyframes slider{0%{',$minOutput);
		}
		
		$minOutput = str_replace('ovpsrc.generalvideo.ru','ovplayer.ru',$minOutput);
		file_put_contents($OutPath . $item, $minOutput);
		
		$sizeOutCur = countBytes($src);
		$sizePackedCur = countBytes($minOutput);
		if($sizePackedCur == '0'){$sizePackedCur = '1';}
		$proc = round(($sizeOutCur/$sizePackedCur)*100-100, 2);
		
		$sizeOut += $sizeOutCur;
		$sizePacked += $sizePackedCur;
		$countFile++;
		
		$a .= '<tr><td>'.$SrcPath.$item.'</td><td>'.$sizeOutCur.'</td><td>'.$OutPath.$item.'</td><td>'.$sizePackedCur.'</td><td>'.
				($sizeOutCur-$sizePackedCur).' / '.$proc.'</td><td>'.$time.' s.</td>'.'</tr>';
	}
	return $a;
}

?>

<!doctype html>
<html>
<head><meta charset="utf-8"><title>***</title>
<link rel="stylesheet" type="text/css" href="/style/style.css" />
<style>
 table{font-size:.7em;margin:0 auto;width:96%;}
 tr:hover{background-color:rgba(0,0,0,.25);}
</style></head><body>
<table>
	<tr><th>out script</th><th>out size (bytes)</th><th>packed in</th><th>packed size (bytes)</th><th>%</th><th>time</th></tr>
<?php
	echo m(dirname(__FILE__).'', '../ovplayer.ru', 'html');
	echo m(dirname(__FILE__).'/style', '../ovplayer.ru/style', 'css');
	
	echo m(dirname(__FILE__).'/javascript', '../ovplayer.ru/javascript', 'js');
	echo m(dirname(__FILE__).'/ovplayer/gm_firefox/latest/', '../ovplayer.ru/ovplayer/gm_firefox/latest/', 'js');
	echo m(dirname(__FILE__).'/ovplayer/gm_firefox/latest/modules/', '../ovplayer.ru/ovplayer/gm_firefox/latest/modules/', 'js');
?>
<tr style='text-align:right;'><th><?=$countFile?></th><th><?=$sizeOut?></th><th>%</th><th><?=$sizePacked?></th><th><?php
	echo $sizeOut-$sizePacked;
	echo ' / ';
	echo round(($sizeOut/$sizePacked)*100-100, 2);
?></th><th><?=$t?> s.</th></tr>
</table></body></html>